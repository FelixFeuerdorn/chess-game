package de.hsos.swa.chessgame.shared.control;

/**
 * @author Nils Helming
 **/
public class UserNameChangedEvent {
    public long userid;
    public String oldUsername;
    public String newUsername;

    public UserNameChangedEvent(long userid, String oldUsername, String newUsername) {
        this.userid = userid;
        this.oldUsername = oldUsername;
        this.newUsername = newUsername;
    }
}
