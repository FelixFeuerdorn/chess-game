package de.hsos.swa.chessgame.shared.control;

/**
 * @author Nils Helming
 **/
public interface UserAccess {
    /**
     * Returns the User-ID of a User, referred by his username.
     *
     * @param username The username of the User.
     * @return The User-ID of the User. '-1' if the User does not exist.
     */
    public long getUserId(String username);

    /**
     * Returns the username of a User, referred by his User-ID.
     *
     * @param userId The User-ID of the User.
     * @return The username of the User. 'null' if the User does not exist.
     */
    public String getUserName(long userId);

    /**
     * Returns whether a User is restricted to only read-actions.
     *
     * @param userId The User-ID of the User.
     * @return 'true' if the User is restricted to read-actions. 'false' if the
     * User is allowed to perform all (usual) actions. 'true' if the User does
     * not exist.
     */
    public boolean isRestricted(long userId);

    /**
     * Returns whether a User is allowed any actions.
     *
     * @param userId The User-ID of the User.
     * @return 'true' if the User is banned from performing any actions. 'false'
     * if the User is allowed to perform all (usual) actions. 'true' if the
     * User does not exist.
     */
    public boolean isBanned(long userId);
}