package de.hsos.swa.chessgame.shared.boundary;

import java.net.URI;

/**
 * @author Nils Helming
 **/
public interface GameLinkRetriever {
    public URI getGameLink(long gameid);
}
