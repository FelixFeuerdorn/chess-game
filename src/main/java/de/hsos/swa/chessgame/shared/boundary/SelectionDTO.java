package de.hsos.swa.chessgame.shared.boundary;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Nils Helming
 **/
public class SelectionDTO<T> {
    public Map<String, URI> links;
    public int totalElements;

    public int firstSelected;
    public int lastSelected;

    public List<T> elements;

    public SelectionDTO(){}

    public <R> SelectionDTO<R> map(Function<? super T, ? extends R> mapper){
        SelectionDTO<R> selectionDTO = new SelectionDTO<>();
        selectionDTO.links = links;
        selectionDTO.totalElements = totalElements;

        selectionDTO.firstSelected = firstSelected;
        selectionDTO.lastSelected = lastSelected;

        selectionDTO.elements = elements.stream()
            .map(mapper)
            .collect(Collectors.toList());
        return selectionDTO;
    }
}