package de.hsos.swa.chessgame.shared.boundary;

import java.util.List;

import javax.enterprise.context.RequestScoped;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class Paginator {
    /**
     * Returns a PageDTO with the given parameters. The PageDTO contains links
     * to the next and previous pages.
     *
     * @param <T> The type of the elements in the page.
     * @param source The source of the elements.
     * @param page The page number.
     * @param count The number of elements per page.
     * @param pageMethod The method of the resource the URI should refer to.
     * @param linkGenerator The Link Generator for the type T.
     * @return A PageDTO with the given parameters.
     * @throws IllegalArgumentException If the page number is negative.
     * @throws IllegalArgumentException If the count is negative.
     */
    public static <T> PageDTO<T> get(List<T> source, int page, int count,
            LinkGenerator<T> linkGenerator, String pageMethod, Object... args){
        if(source == null)
            throw new NullPointerException("source must not be null");
        if(page < 1)
            throw new IllegalArgumentException("page must be greater than 0");
        if(count < 1)
            throw new IllegalArgumentException("count must be greater than 0");


        int source_size=source.size();
        int start = (page - 1) * count;
        int end = Math.min(start + count, source_size);

        if(start>end){ //subList will return an empty list.
            start = end;
        }

        int lastPage = Math.max(0,(int) Math.ceil((double) source_size / count));

        PageDTO<T> pageDTO = new PageDTO<>();
        pageDTO.elements = source.subList(start, end);
        pageDTO.totalPages = lastPage;
        pageDTO.totalElements = source_size;
        pageDTO.page = page;
        pageDTO.count = count;

        /* Add all Links */
        pageDTO.links = linkGenerator.getCollectionLinks();
        pageDTO.links.put("self",
            linkGenerator.getPageLink(pageMethod, page, count, args));

        if(lastPage > 0){ //if there is at least a single page..
            pageDTO.links.put("first",
                linkGenerator.getPageLink(pageMethod, 1, count, args));
            pageDTO.links.put("last",
                linkGenerator.getPageLink(pageMethod, lastPage, count, args));
        }

        if(page > 1){ //if there is a previous page
            pageDTO.links.put("previous",
                linkGenerator.getPageLink(pageMethod, page-1, count, args));
        }

        if(page < lastPage){ //if there is a next page
            pageDTO.links.put("next",
                linkGenerator.getPageLink(pageMethod, page+1, count, args));
        }

        return pageDTO;
    }
}
