package de.hsos.swa.chessgame.shared.control;

/**
 * @author Björn Droste
 **/
public class GameDataEvent {

    private final Long userid;

    private final String resultname;

    private final int value;

    public GameDataEvent(Long userid, String resultname, int value) {
        this.userid = userid;
        this.resultname = resultname;
        this.value = value;
    }

    public Long getUserid() {
        return userid;
    }

    public String getResultname() {
        return resultname;
    }

    public int getValue() {
        return value;
    }
}