package de.hsos.swa.chessgame.shared.boundary;

import java.net.URI;

/**
 * @author Nils Helming
 **/
public interface UserLinkRetriever {
    public URI getUserLink(long userid);
}
