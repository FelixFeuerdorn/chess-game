package de.hsos.swa.chessgame.shared.boundary;

import java.net.URI;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author Nils Helming
 **/
public interface LinkGenerator<T> {

    /**
     * Retrieves the URI usually called "self".
     *
     * @param target The Object which shall be accessible by the URI.
     * @return the URI pointing towards the Object.
     */
    public URI getRequestLink(@Valid @NotNull T target);

    /**
     * Retrieves all Links related to a specific Element of type T. Examples:
     * "self", "update", "delete"
     *
     * @param target The Specific Element, for which URI's should be provided.
     * @return A Map Containing URI's for actions on a Specific Object of type T.
     */
    public Map<String,URI> getEntityLinks(@Valid @NotNull T target);

    /**
     * Creates a URI to a page of a specific Method within the resource of this
     * type. The page number is given by the {@code page} parameter. The
     * {@code count} parameter specifies the number of items per page. The
     * {@code pageMethod} parameter specifies the method of the resource the URI
     * should refer to.
     *
     * @param pageMethod the method of the resource the URI should refer to
     * @param page      the page number
     * @param count    the number of items per page
     * @return a URI to the specified page
     */
    public URI getPageLink(String pageMethod, int page, int count, Object... buildArgs);

    /**
     * Retrieves all Links related to a collection of T. Example: "create"
     *
     * @return A Map Containing URI's for actions on a Collection of T.
     */
    public Map<String,URI> getCollectionLinks();
}
