package de.hsos.swa.chessgame.shared.boundary;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Nils Helming
 **/
public class PageDTO<T> {
    public Map<String, URI> links;
    public int page;
    public int count;
    public int totalPages;
    public int totalElements;

    public List<T> elements;

    public PageDTO(){}

    public <R> PageDTO<R> map(Function<? super T, ? extends R> mapper){
        PageDTO<R> pageDTO = new PageDTO<>();
        pageDTO.links = links;
        pageDTO.page = page;
        pageDTO.count = count;
        pageDTO.totalPages = totalPages;
        pageDTO.totalElements = totalElements;
        pageDTO.elements = elements.stream()
            .map(mapper)
            .collect(Collectors.toList());
        return pageDTO;
    }
}
