package de.hsos.swa.chessgame.shared.boundary;

import java.util.List;

import javax.enterprise.context.RequestScoped;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class Selector {

    /**
     * Creates a SelectionDTO from a list of elements. The SelectionDTO contains
     * all the elements within the given range between (inclusive) the first and
     * last element. Negative numbers are treated as offsets from the end of the
     * list, such that '-1' is the last element, '-2' is the second last element
     * and so on. If 'first > last' there will be no elements within the
     * selection. The SelectionDTO contains all Collection specific links of
     * this type provided by the LinkGenerator.
     *
     * @param <T> The type of the elements in the List/Selection.
     * @param source The source-list of the elements.
     * @param first The first element of the range.
     * @param last The last element of the range.
     * @param linkGenerator The LinkGenerator for the type T.
     * @return A SelectionDTO as described above.
     */
    public static <T> SelectionDTO<T> select(List<T> source, int first, int last,
                LinkGenerator<T> linkGenerator){
        if(source == null)
            throw new NullPointerException("source must not be null");


        int source_size=source.size();

        //negative values are translated into their offset from the end
        //for exapmple '-1' refers to the last element 'source_size'
        //be aware that the first element is 1. This difference will have to
        //be accounted for later.
        if(first < 1){ //0 gets treated as if it was negative.
            first = source_size+1 + first; //'first' is negative!!
            if(first < 1) //if its still negative, we went out of bounds.
                first = 1;
        }
        if(first > source_size) //there is no special rule for too big numbers.
            first = source_size; //it's just out of bounds...

        //repeat what we just did with 'first' with 'last'.
        if(last < 1){
            last = source_size - last;
            if(last < 1)
                last = 1;
        }
        if(last > source_size)
            last = source_size;

        //now... we did many fancy things... if the requested range is for some
        //reason reversed, we could either swap the first and last values or
        //just return an empty list. This is something that can happen in any
        //case. Thus we choose to return an empty list. Which is only possible
        //with 0 for 'first' and 'last'.
        if(first > last){
            first = 0;
            last = 0;
        }

        List<T> elements;
        if(first == 0 || last == 0) //if one is 0, the other should be too,
            elements = List.of(); //but you can never be too careful.
        else
            elements = source.subList(first-1, last); //first argument is 0-based.


        SelectionDTO<T> selectionDTO = new SelectionDTO<>();
        selectionDTO.elements = elements;
        selectionDTO.totalElements = source_size;
        selectionDTO.firstSelected = first;
        selectionDTO.lastSelected = last;
        selectionDTO.links = linkGenerator.getCollectionLinks();

        return selectionDTO;
    }

}
