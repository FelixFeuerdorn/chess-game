package de.hsos.swa.chessgame.shared.control;

/**
 * @author Nils Helming
 **/
public class UserDeletedEvent {
    public long userid;
    public String username;

    public UserDeletedEvent(long userid, String username) {
        this.userid = userid;
        this.username = username;
    }
}
