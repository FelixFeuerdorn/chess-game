package de.hsos.swa.chessgame.ChessManagement.entity;

/**
 * @author Björn Droste
 **/
public class ChessFigure {
    
    private final ChessPiece chessPiece;
    private final ChessColor chessColor;
    private boolean startState = true;
    
    public ChessFigure(ChessPiece chessPiece, ChessColor chessColor) {
        this.chessPiece = chessPiece;
        this.chessColor = chessColor;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chessColor == null) ? 0 : chessColor.hashCode());
        result = prime * result + ((chessPiece == null) ? 0 : chessPiece.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChessFigure other = (ChessFigure) obj;
        if (chessColor != other.chessColor)
            return false;
        if (chessPiece != other.chessPiece)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ChessFigure [chessColor=" + chessColor + ", chessPiece=" + chessPiece + "]";
    }

    public ChessPiece getChessPiece() {
        return chessPiece;
    }

    public ChessColor getChessColor() {
        return chessColor;
    }

    public boolean getStartState() {
        return startState;
    }

    public void setStartState(boolean startState) {
        this.startState = startState;
    }
}
