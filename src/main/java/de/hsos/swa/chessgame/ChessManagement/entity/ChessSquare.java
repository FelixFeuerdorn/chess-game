package de.hsos.swa.chessgame.ChessManagement.entity;

/**
 * @author Björn Droste
 **/
public enum ChessSquare {
    A8(0,7), B8(1,7), C8(2,7), D8(3,7), E8(4,7), F8(5,7), G8(6,7), H8(7,7),
    A7(0,6), B7(1,6), C7(2,6), D7(3,6), E7(4,6), F7(5,6), G7(6,6), H7(7,6), 
    A6(0,5), B6(1,5), C6(2,5), D6(3,5), E6(4,5), F6(5,5), G6(6,5), H6(7,5), 
    A5(0,4), B5(1,4), C5(2,4), D5(3,4), E5(4,4), F5(5,4), G5(6,4), H5(7,4),  
    A4(0,3), B4(1,3), C4(2,3), D4(3,3), E4(4,3), F4(5,3), G4(6,3), H4(7,3),
    A3(0,2), B3(1,2), C3(2,2), D3(3,2), E3(4,2), F3(5,2), G3(6,2), H3(7,2), 
    A2(0,1), B2(1,1), C2(2,1), D2(3,1), E2(4,1), F2(5,1), G2(6,1), H2(7,1), 
    A1(0,0), B1(1,0), C1(2,0), D1(3,0), E1(4,0), F1(5,0), G1(6,0), H1(7,0);

    public final int column;
    public final int row;

    private ChessSquare(int column, int row) {
        this.column = column;
        this.row = row;
    }

    // ChessSquare anhand von Row und Column erkennen
    public static ChessSquare giveChessSquare(int column, int row){
        switch(column){
            case 0:{
                switch(row){
                    case 0:{return ChessSquare.A1;}
                    case 1:{return ChessSquare.A2;}
                    case 2:{return ChessSquare.A3;}
                    case 3:{return ChessSquare.A4;}
                    case 4:{return ChessSquare.A5;}
                    case 5:{return ChessSquare.A6;}
                    case 6:{return ChessSquare.A7;}
                    case 7:{return ChessSquare.A8;}
                    default: return null;
                }
            }
            case 1:{
                switch(row){
                    case 0:{return ChessSquare.B1;}
                    case 1:{return ChessSquare.B2;}
                    case 2:{return ChessSquare.B3;}
                    case 3:{return ChessSquare.B4;}
                    case 4:{return ChessSquare.B5;}
                    case 5:{return ChessSquare.B6;}
                    case 6:{return ChessSquare.B7;}
                    case 7:{return ChessSquare.B8;}
                    default: return null;
                }
            }
            case 2:{
                switch(row){
                    case 0:{return ChessSquare.C1;}
                    case 1:{return ChessSquare.C2;}
                    case 2:{return ChessSquare.C3;}
                    case 3:{return ChessSquare.C4;}
                    case 4:{return ChessSquare.B5;}
                    case 5:{return ChessSquare.C6;}
                    case 6:{return ChessSquare.C7;}
                    case 7:{return ChessSquare.C8;}
                    default: return null;
                }
            }
            case 3:{
                switch(row){
                    case 0:{return ChessSquare.D1;}
                    case 1:{return ChessSquare.D2;}
                    case 2:{return ChessSquare.D3;}
                    case 3:{return ChessSquare.D4;}
                    case 4:{return ChessSquare.D5;}
                    case 5:{return ChessSquare.D6;}
                    case 6:{return ChessSquare.D7;}
                    case 7:{return ChessSquare.D8;}
                    default: return null;
                }
            }
            case 4:{
                switch(row){
                    case 0:{return ChessSquare.E1;}
                    case 1:{return ChessSquare.E2;}
                    case 2:{return ChessSquare.E3;}
                    case 3:{return ChessSquare.E4;}
                    case 4:{return ChessSquare.E5;}
                    case 5:{return ChessSquare.E6;}
                    case 6:{return ChessSquare.E7;}
                    case 7:{return ChessSquare.E8;}
                    default: return null;
                }
            }
            case 5:{
                switch(row){
                    case 0:{return ChessSquare.F1;}
                    case 1:{return ChessSquare.F2;}
                    case 2:{return ChessSquare.F3;}
                    case 3:{return ChessSquare.F4;}
                    case 4:{return ChessSquare.F5;}
                    case 5:{return ChessSquare.F6;}
                    case 6:{return ChessSquare.F7;}
                    case 7:{return ChessSquare.F8;}
                    default: return null;
                }
            }
            case 6:{
                switch(row){
                    case 0:{return ChessSquare.G1;}
                    case 1:{return ChessSquare.G2;}
                    case 2:{return ChessSquare.G3;}
                    case 3:{return ChessSquare.G4;}
                    case 4:{return ChessSquare.G5;}
                    case 5:{return ChessSquare.G6;}
                    case 6:{return ChessSquare.G7;}
                    case 7:{return ChessSquare.G8;}
                    default: return null;
                }
            }
            case 7:{
                switch(row){
                    case 0:{return ChessSquare.H1;}
                    case 1:{return ChessSquare.H2;}
                    case 2:{return ChessSquare.H3;}
                    case 3:{return ChessSquare.H4;}
                    case 4:{return ChessSquare.H5;}
                    case 5:{return ChessSquare.H6;}
                    case 6:{return ChessSquare.H7;}
                    case 7:{return ChessSquare.H8;}
                    default: return null;
                }
            }
            default: return null;
        }
    }
}
