package de.hsos.swa.chessgame.ChessManagement.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Björn Droste
 **/
public class ChessBoard {

    private Map<ChessSquare,ChessFigure> square;

    public ChessBoard() {
        this.square = this.fillMap();
    }

    public ChessFigure getSquare(ChessSquare square) {
        return this.square.get(square);
    }

    public void setSquare(ChessSquare square, ChessFigure figure) {
        if(figure == null) {
            this.square.remove(square);
        } else {
            this.square.put(square, figure);
        }
    }

    private Map<ChessSquare,ChessFigure> fillMap(){
        Map<ChessSquare,ChessFigure> tofill = new HashMap<>();

        tofill.put(ChessSquare.A1, new ChessFigure(ChessPiece.Rook,ChessColor.White));
        tofill.put(ChessSquare.B1, new ChessFigure(ChessPiece.Knight,ChessColor.White));
        tofill.put(ChessSquare.C1, new ChessFigure(ChessPiece.Bishop,ChessColor.White));
        tofill.put(ChessSquare.D1, new ChessFigure(ChessPiece.Queen,ChessColor.White));
        tofill.put(ChessSquare.E1, new ChessFigure(ChessPiece.King,ChessColor.White));
        tofill.put(ChessSquare.F1, new ChessFigure(ChessPiece.Bishop,ChessColor.White));
        tofill.put(ChessSquare.G1, new ChessFigure(ChessPiece.Knight,ChessColor.White));
        tofill.put(ChessSquare.H1, new ChessFigure(ChessPiece.Rook,ChessColor.White));

        tofill.put(ChessSquare.A2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.B2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.C2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.D2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.E2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.F2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.G2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.H2, new ChessFigure(ChessPiece.Pawn,ChessColor.White));

        tofill.put(ChessSquare.A7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.B7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.C7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.D7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.E7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.F7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.G7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.H7,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));

        tofill.put(ChessSquare.A8,  new ChessFigure(ChessPiece.Rook,ChessColor.Black));
        tofill.put(ChessSquare.B8,  new ChessFigure(ChessPiece.Knight,ChessColor.Black));
        tofill.put(ChessSquare.C8,  new ChessFigure(ChessPiece.Bishop,ChessColor.Black));
        tofill.put(ChessSquare.D8,  new ChessFigure(ChessPiece.Queen,ChessColor.Black));
        tofill.put(ChessSquare.E8,  new ChessFigure(ChessPiece.King,ChessColor.Black));
        tofill.put(ChessSquare.F8,  new ChessFigure(ChessPiece.Bishop,ChessColor.Black));
        tofill.put(ChessSquare.G8,  new ChessFigure(ChessPiece.Knight,ChessColor.Black));
        tofill.put(ChessSquare.H8,  new ChessFigure(ChessPiece.Rook,ChessColor.Black));

        return tofill;
    }

    public void myChessBoard(){
        Map<ChessSquare,ChessFigure> tofill = new HashMap<>();
        ChessFigure rook = new ChessFigure(ChessPiece.Rook,ChessColor.White);
        rook.setStartState(false);

        tofill.put(ChessSquare.A1,  new ChessFigure(ChessPiece.Rook,ChessColor.White));
        tofill.put(ChessSquare.H1,  rook);
        tofill.put(ChessSquare.E1,  new ChessFigure(ChessPiece.King,ChessColor.White));

        tofill.put(ChessSquare.B2,  new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.F2,  new ChessFigure(ChessPiece.Queen,ChessColor.White));

        tofill.put(ChessSquare.E3,  new ChessFigure(ChessPiece.Knight,ChessColor.White));

        tofill.put(ChessSquare.A4,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.C4,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.G4,  new ChessFigure(ChessPiece.Bishop,ChessColor.White));

        tofill.put(ChessSquare.A5,  new ChessFigure(ChessPiece.Bishop,ChessColor.White));
        tofill.put(ChessSquare.E5,  new ChessFigure(ChessPiece.Bishop,ChessColor.Black));
        tofill.put(ChessSquare.C5,  new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.H5,  new ChessFigure(ChessPiece.Pawn,ChessColor.White));

        tofill.put(ChessSquare.A6,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.B6,  new ChessFigure(ChessPiece.Pawn,ChessColor.Black));
        tofill.put(ChessSquare.D6,  new ChessFigure(ChessPiece.Rook,ChessColor.Black));
        tofill.put(ChessSquare.H6,  new ChessFigure(ChessPiece.Knight,ChessColor.Black));

        tofill.put(ChessSquare.B7,  new ChessFigure(ChessPiece.King,ChessColor.Black));
        tofill.put(ChessSquare.E7,  new ChessFigure(ChessPiece.Pawn,ChessColor.White));
        tofill.put(ChessSquare.G7,  new ChessFigure(ChessPiece.Queen,ChessColor.Black));

        this.square = tofill;
    }
}
