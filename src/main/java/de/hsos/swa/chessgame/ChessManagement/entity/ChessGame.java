package de.hsos.swa.chessgame.ChessManagement.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

/**
 * @author Nils Helming
 **/
public class ChessGame {
    private final long gameid;
    private final String host;
    private final String opponent;
    @NotNull private final ChessColor hostcolor;
    private final boolean isPublic;

    @PastOrPresent private final LocalDateTime createdAt;

    @ValidMoveSequece private final List<@ValidMove ChessMove> moves;

    public ChessGame(
            long gameid, String host, String opponent, ChessColor hostcolor,
            boolean isPublic, LocalDateTime createdAt, List<ChessMove> moves) {
        this.gameid = gameid;
        this.host = host;
        this.opponent = opponent;
        this.hostcolor = hostcolor;
        this.isPublic = isPublic;

        this.createdAt = createdAt;

        this.moves = new ArrayList<>();
        for(ChessMove move : moves) {
            this.moves.add(
                move.setGameid(gameid).setMovenr(this.moves.size() + 1)
            );
        }
    }

    public ChessGame(
            long gameid, String host, String opponent, ChessColor hostcolor,
            boolean isPublic, LocalDateTime createdAt) {
        this.gameid = gameid;
        this.host = host;
        this.opponent = opponent;
        this.hostcolor = hostcolor;
        this.isPublic = isPublic;

        this.createdAt = createdAt;

        this.moves = new ArrayList<>();
    }

    public long getGameid() {
        return gameid;
    }

    public String getHost() {
        return host;
    }

    public String getOpponent() {
        return opponent;
    }

    public ChessColor getHostcolor() {
        return hostcolor;
    }

    public ChessColor getOpponentcolor() {
        return ChessColor.getOpposite(hostcolor);
    }

    public boolean isPublic() {
        return isPublic;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public List<ChessMove> getMoves() {
        return new ArrayList<>(moves);
    }

    //----------------- IMMUTABLE SETTERS -----------------

    public ChessGame setGameid(long gameid) {
        return new ChessGame(gameid, this.host, this.opponent,
            this.hostcolor, this.isPublic, this.createdAt, this.moves);
    }

    public ChessGame setHost(String host) {
        return new ChessGame(this.gameid, host, this.opponent,
            this.hostcolor, this.isPublic, this.createdAt, this.moves);
    }

    public ChessGame setOpponent(String opponent) {
        return new ChessGame(this.gameid, this.host, opponent,
            this.hostcolor, this.isPublic, this.createdAt, this.moves);
    }

    public ChessGame setHostcolor(ChessColor hostcolor) {
        return new ChessGame(this.gameid, this.host, this.opponent,
            hostcolor, this.isPublic, this.createdAt, this.moves);
    }

    public ChessGame setIsPublic(boolean isPublic) {
        return new ChessGame(this.gameid, this.host, this.opponent,
            this.hostcolor, isPublic, this.createdAt, this.moves);
    }

    public ChessGame addMove(ChessMove move) {
        List<ChessMove> newMoves = new ArrayList<>(this.moves);
        newMoves.add(move); //gameid and movenr are adjusted in constructor!
        return new ChessGame(this.gameid, this.host, this.opponent,
            this.hostcolor, this.isPublic, this.createdAt, newMoves);
    }
}
