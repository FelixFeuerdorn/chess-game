package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import de.hsos.swa.chessgame.ChessManagement.control.ChessService;
import de.hsos.swa.chessgame.ChessManagement.control.MoveAddedEvent;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessBoard;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessLogic;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.operators.multi.processors.UnicastProcessor;

@ApplicationScoped
@Path("/subscribe")
@Produces("application/json")
public class ChessSubscribtion {
    //basically a Multimap from gameid to UnicastProcessor<Response>
    private ConcurrentMap<Long, Set<UnicastProcessor<Response>>> subscribers
            = new ConcurrentHashMap<>();

    @Inject ChessMoveOut.Converter chessMoveOutConverter;
    @Inject ChessService chessService;
    @Inject SecurityIdentity securityIdentity;
    @Inject ChessGameAccess gameAccess;
    @Inject ChessResource chessResource;
    @Inject ChessLogic chessLogic;

    @GET
    @Authenticated
    @Path("/{gameid}/{zugnr}")
    public Uni<Response> subscribe(
            @PathParam("gameid") long gameid,
            @PathParam("zugnr") int zugnr) {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Uni.createFrom().item(
                Response.status(Response.Status.NOT_FOUND).build());
        }else if(!gameAccess.userCanSee(securityIdentity, game.get())) {
            return Uni.createFrom().item(
                Response.status(Response.Status.FORBIDDEN).build());
        }

        List<ChessMove> moves = game.get().getMoves();

        if(zugnr < 1 || zugnr > moves.size()+1) //Out of Bounds (<1 or >nextMove)
            return Uni.createFrom().item(
                Response.status(Response.Status.BAD_REQUEST).build());

        if(zugnr <= moves.size()) //Move already exists
            return Uni.createFrom().item(
                chessResource.getMove(gameid, zugnr));

        //Move is the next to be created!

        UnicastProcessor<Response> subscriber = UnicastProcessor.create();
        return this.put(gameid, subscriber).toUni()
            .onCancellation().invoke(() -> {
                this.remove(gameid, subscriber);
            });
    }

    void onEvent(@Observes MoveAddedEvent addedEvent){
        this.popAll(addedEvent.getGame().getGameid()).forEach(subscriber -> {
            ChessMove move = addedEvent.getMove();
            ChessBoard before = chessLogic.generateChessBoard(
                addedEvent.getGame(), move.getMovenr()-1);
            ChessBoard after = chessLogic.generateChessBoard(
                addedEvent.getGame(), move.getMovenr());

            subscriber.onNext(Response.ok(
                chessMoveOutConverter.toDTO(move, before, after)).build());
            subscriber.onComplete();
        });
    }

    //--------------------- Map - Operations ---------------------

    private UnicastProcessor<Response> put(
            long gameid, UnicastProcessor<Response> subscriber) {
        subscribers.computeIfAbsent(gameid, k -> new HashSet<>()).add(subscriber);
        return subscriber;
    }

    private Set<UnicastProcessor<Response>> popAll(long gameid) {
        Set<UnicastProcessor<Response>> result = subscribers.remove(gameid);
        return (result == null) ? new HashSet<>() : result;
    }

    private void remove(long gameid, UnicastProcessor<Response> subscriber) {
        subscribers.computeIfPresent(gameid, (id, set) -> {
            set.remove(subscriber);
            return (set.isEmpty()) ? null : set;
        });
    }
}
