package de.hsos.swa.chessgame.ChessManagement.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.Valid;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessCatalog;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessColor;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessFigure;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessLogic;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMoveType;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessPiece;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessSquare;
import de.hsos.swa.chessgame.shared.control.UserAccess;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class ChessService {

    @Inject ChessCatalog catalog;
    @Inject UserAccess userAccess;
    @Inject ChessLogic chessLogic;
    @Inject Event<MoveAddedEvent> moveAddedEvent;

    public List<@Valid ChessGame> getGames() {
        return catalog.getGames();
    }

    private boolean gameContainsUser(@Valid ChessGame game, String user){
        return game.getHost() == user || game.getOpponent() == user;
    }

    public List<@Valid ChessGame> getParticipantGames(String user) {
        return getGames().stream()
            .filter(game -> gameContainsUser(game, user))
            .collect(Collectors.toList());
    }

    public Optional<@Valid ChessGame> getGame(long gameid) {
        return Optional.ofNullable(catalog.getGame(gameid));
    }

    public Optional<@Valid ChessGame> createGame(String host, String opponent, ChessColor hostcolor, boolean isPublic) {
        if(userAccess.getUserId(host) < 0)
            return Optional.empty();
        if(userAccess.getUserId(opponent) < 0)
            return Optional.empty();

        ChessGame game = new ChessGame(-1, host, opponent, hostcolor,
            isPublic, LocalDateTime.now());
        game = catalog.createGame(game);
        return Optional.ofNullable(game);
    }

    public Optional<@Valid ChessGame> updateOpponent(long gameid, String opponent) {
        if(userAccess.getUserId(opponent) < 0)
            return Optional.empty();

        Optional<ChessGame> optGame = getGame(gameid);
        if(!optGame.isPresent()){
            return Optional.empty();
        }

        ChessGame game = optGame.get();
        game = catalog.updateGameOptions(gameid, game.setOpponent(opponent));
        return Optional.ofNullable(game);
    }

    public Optional<@Valid ChessGame> updateHostColor(long gameid, ChessColor hostcolor) {
        Optional<ChessGame> optGame = getGame(gameid);
        if(!optGame.isPresent()){
            return Optional.empty();
        }

        ChessGame game = optGame.get();
        game = catalog.updateGameOptions(gameid, game.setHostcolor(hostcolor));
        return Optional.ofNullable(game);
    }

    public Optional<@Valid ChessGame> updatePublic(long gameid, boolean isPublic) {
        Optional<ChessGame> optGame = getGame(gameid);
        if(!optGame.isPresent()){
            return Optional.empty();
        }

        ChessGame game = optGame.get();
        game = catalog.updateGameOptions(gameid, game.setIsPublic(isPublic));
        return Optional.ofNullable(game);
    }

    public boolean deleteGame(long gameid) {
        return catalog.deleteGame(gameid);
    }

    public Optional<@Valid ChessGame> addMove(String user, long gameid, ChessSquare from, ChessSquare to,
            ChessMoveType moveType, ChessPiece promotionPiece) {

        if(userAccess.getUserId(user) < 0)
            return Optional.empty();

        Optional<ChessGame> optGame = getGame(gameid);
        if(!optGame.isPresent())
            return Optional.empty();

        ChessGame game = optGame.get();

        ChessFigure figure = chessLogic.getFigureAt(game, from);
        if(figure == null || figure.getChessColor() != chessLogic.getNextMoveColor(game))
            return Optional.empty();

        ChessPiece piece = figure.getChessPiece();
        ChessColor color = figure.getChessColor();
        int movenr = chessLogic.getNextMoveNr(game);

        ChessMove move = new ChessMove(user, gameid, movenr, piece,
            color, from, to, LocalDateTime.now(), moveType, promotionPiece);

        if(!chessLogic.isValidNextMove(game, move))
            return Optional.empty();

        game = catalog.addMove(gameid, move);
        if(game != null){
            move = game.getMoves().get(game.getMoves().size() - 1);
            moveAddedEvent.fire(new MoveAddedEvent(move, game));
        }

        return Optional.ofNullable(game);
    }

    public String getNextMoveUser(long gameid){
        Optional<ChessGame> optGame = getGame(gameid);
        if(!optGame.isPresent())
            return null;

        ChessGame game = optGame.get();
        return chessLogic.getNextMoveColor(game) == game.getHostcolor() ?
            game.getHost() : game.getOpponent();
    }

}
