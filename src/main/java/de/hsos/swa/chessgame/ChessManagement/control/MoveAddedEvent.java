package de.hsos.swa.chessgame.ChessManagement.control;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;

public class MoveAddedEvent {
    private final ChessMove move;
    private final ChessGame game;

    public MoveAddedEvent(ChessMove move, ChessGame game) {
        this.move = move;
        this.game = game;
    }

    public ChessMove getMove() {
        return move;
    }

    public ChessGame getGame() {
        return game;
    }
}
