package de.hsos.swa.chessgame.ChessManagement.boundary;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessMoveType;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessPiece;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessSquare;

/**
 * @author Nils Helming
 **/
public class ChessMoveIn {
    public ChessSquare from;
    public ChessSquare to;

    public ChessMoveType moveType;
    public ChessPiece promotionPiece;
}
