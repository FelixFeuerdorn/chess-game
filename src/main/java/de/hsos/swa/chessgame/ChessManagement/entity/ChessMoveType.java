package de.hsos.swa.chessgame.ChessManagement.entity;

public enum ChessMoveType {
    NORMAL, EN_PASSANT, PROMOTION, CASTLING
}
