package de.hsos.swa.chessgame.ChessManagement.entity;

/**
 * @author Nils Helming
 **/
public enum ChessPiece {
    Pawn, Rook, Knight, Bishop, Queen, King;
}
