package de.hsos.swa.chessgame.ChessManagement.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import de.hsos.swa.chessgame.ChessManagement.entity.ValidMove.MoveValidator;

@Constraint(validatedBy = {MoveValidator.class})
@Target({
    ElementType.METHOD,
    ElementType.FIELD,
    ElementType.ANNOTATION_TYPE,
    ElementType.CONSTRUCTOR,
    ElementType.PARAMETER,
    ElementType.TYPE_USE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidMove {
    String message() default "Invalid move";
    Class<?>[] groups() default {};
    Class<? extends ConstraintValidator<ValidMove, ?>>[] payload() default {};

    public static class MoveValidator implements ConstraintValidator<ValidMove, ChessMove> {

        @Override
        public boolean isValid(ChessMove move, ConstraintValidatorContext context) {
            //Default-Violations sollen nicht erzeugt werden
            context.disableDefaultConstraintViolation();
            boolean result = true;

            //Es darf maximal einer der 'Special attributes' gesetzt sein
            if(move.isCastling() && move.isEnPassant()){
                context
                    .buildConstraintViolationWithTemplate("Es darf maximal einer der 'Special attributes' gesetzt sein")
                    .addPropertyNode("isCastling")
                    .addPropertyNode("isEnPassant")
                    .addConstraintViolation();
                result = false;
            }

            if(move.isCastling() && move.isPromotion()){
                context
                    .buildConstraintViolationWithTemplate("Es darf maximal einer der 'Special attributes' gesetzt sein")
                    .addPropertyNode("isCastling")
                    .addPropertyNode("isPromotion")
                    .addConstraintViolation();
                result = false;
            }

            if(move.isEnPassant() && move.isPromotion()){
                context
                    .buildConstraintViolationWithTemplate("Es darf maximal einer der 'Special attributes' gesetzt sein")
                    .addPropertyNode("isEnPassant")
                    .addPropertyNode("isPromotion")
                    .addConstraintViolation();
                result = false;
            }

            //Falls 'isPromotion' gesetzt ist, muss 'promotionPiece' gesetzt sein
            if(move.isPromotion() && move.getPromotionPiece() == null){
                context
                    .buildConstraintViolationWithTemplate("Falls 'isPromotion' gesetzt ist, muss 'promotionPiece' gesetzt sein")
                    .addPropertyNode("promotionPiece")
                    .addConstraintViolation();
                result = false;
            }

            return result;
        }

    }
}
