package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessColor;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessLogic;
import de.hsos.swa.chessgame.shared.boundary.LinkGenerator;
import de.hsos.swa.chessgame.shared.boundary.UserLinkRetriever;
import de.hsos.swa.chessgame.shared.control.UserAccess;

/**
 * @author Nils Helming
 **/
public class ChessGameOut {
    public long gameid;
    public String hostName;
    public String opponentName;
    public ChessColor hostcolor;
    public ChessColor opponentcolor;
    public boolean isPublic;
    public LocalDateTime createdAt;

    public String board;
    public int totalMoves;
    public Map<String, URI> links;

    @RequestScoped
    public static class Converter{
        @Inject ChessMoveOut.Converter moveConverter;
        @Inject LinkGenerator<ChessGame> gameLinks;
        @Inject UserLinkRetriever userLinks;
        @Inject UserAccess userAccess;
        @Inject ChessLogic chessLogic;

        public ChessGameOut toDTO(@Valid ChessGame chessGame){
            ChessGameOut chessGameOut = new ChessGameOut();
            chessGameOut.gameid = chessGame.getGameid();
            chessGameOut.hostName = chessGame.getHost();
            chessGameOut.opponentName = chessGame.getOpponent();
            chessGameOut.hostcolor = chessGame.getHostcolor();
            chessGameOut.opponentcolor = chessGame.getOpponentcolor();
            chessGameOut.isPublic = chessGame.isPublic();
            chessGameOut.createdAt = chessGame.getCreatedAt();

            chessGameOut.totalMoves = chessGame.getMoves().size();

            chessGameOut.board = BoardStringService.toString(
                chessLogic.generateChessBoard(chessGame));

            long hostid = userAccess.getUserId(chessGame.getHost());
            long opponentid = userAccess.getUserId(chessGame.getOpponent());

            chessGameOut.links = gameLinks.getEntityLinks(chessGame);
            if(hostid > 0)
                chessGameOut.links.put("hostUser",
                    userLinks.getUserLink(hostid));

            if(opponentid > 0)
                chessGameOut.links.put("opponentUser",
                    userLinks.getUserLink(opponentid));

            return chessGameOut;
        }
    }

}
