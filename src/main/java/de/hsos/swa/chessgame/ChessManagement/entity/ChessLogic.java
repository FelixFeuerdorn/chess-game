package de.hsos.swa.chessgame.ChessManagement.entity;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class ChessLogic {

    @Inject ChessMoveCheck chessMoveCheck;

    public boolean isValidNextMove(ChessGame game, ChessMove move) {
        ChessBoard board = generateChessBoard(game);
        ChessMove lastMove = getLastMove(game);

        return chessMoveCheck.check(lastMove, board, move);
    }

    public ChessColor getNextMoveColor(ChessGame game) {
        //white always starts - so, if there are an even amount of moves made,
        //the next player will be white.
        if(game.getMoves().size() % 2 == 0)
            return ChessColor.White;
        else
            return ChessColor.Black;
    }

    public int getNextMoveNr(ChessGame game) {
        return game.getMoves().size() + 1;
    }

    public ChessFigure getFigureAt(ChessGame game, ChessSquare square) {
        ChessBoard board = generateChessBoard(game);
        return board.getSquare(square);
    }

    public ChessBoard generateChessBoard(ChessGame game){
        return generateChessBoard(game, game.getMoves().size());
    }

    public ChessBoard generateChessBoard(ChessGame game, int zugnr){
        ChessBoard board = new ChessBoard();
        List<ChessMove> moves = game.getMoves();

        if(zugnr > moves.size())
            throw new IllegalArgumentException("Zugnummer ist zu gross");

        for(int i = 0; i < zugnr; i++){
            makeMove(board, moves.get(i));
        }

        return board;
    }

    private ChessMove getLastMove(ChessGame game){
        List<ChessMove> moves = game.getMoves();
        if(moves.size() > 0)
            return moves.get(moves.size() - 1);
        else
            return null;
    }

    public void makeMove(ChessBoard board, ChessMove move){
        ChessFigure from = board.getSquare(move.getFrom());
        board.setSquare(move.getFrom(), null);

        from.setStartState(false);
        board.setSquare(move.getTo(), from);

        if(move.isCastling()){
            ChessSquare toSquare = move.getTo();
            ChessSquare fromRook, toRook;

            if(toSquare.column == ChessSquare.C1.column){ //rook A? to D?
                fromRook = ChessSquare.giveChessSquare(ChessSquare.A1.column, toSquare.row);
                toRook = ChessSquare.giveChessSquare(ChessSquare.D1.column, toSquare.row);
            }else{ //rook H? to F?
                fromRook = ChessSquare.giveChessSquare(ChessSquare.H1.column, toSquare.row);
                toRook = ChessSquare.giveChessSquare(ChessSquare.F1.column, toSquare.row);
            }
            ChessFigure rook = board.getSquare(fromRook);
            board.setSquare(fromRook, null);

            rook.setStartState(false);
            board.setSquare(toRook, rook);
        }
        if(move.isEnPassant()){
            ChessSquare fromSquare = move.getFrom();
            ChessSquare toSquare = move.getTo();

            ChessSquare pawnSquare =
                ChessSquare.giveChessSquare(toSquare.column, fromSquare.row);

            board.setSquare(pawnSquare, null);
        }
        if(move.isPromotion()){
            ChessFigure newFigure =
                new ChessFigure(move.getPromotionPiece(), from.getChessColor());
            newFigure.setStartState(false);

            board.setSquare(move.getTo(), newFigure);
        }
    }
}
