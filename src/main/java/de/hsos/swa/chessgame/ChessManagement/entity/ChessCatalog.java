package de.hsos.swa.chessgame.ChessManagement.entity;

import java.util.List;

import javax.validation.Valid;

/**
 * @author Nils Helming
 **/
public interface ChessCatalog {

    public @Valid ChessGame createGame(@Valid ChessGame game);

    public List<@Valid ChessGame> getGames();

    public @Valid ChessGame getGame(long gameid);

    public @Valid ChessGame updateGameOptions(long gameid, @Valid ChessGame update);

    public @Valid ChessGame addMove(long gameid, @Valid ChessMove move);

    public boolean deleteGame(long gameid);
}
