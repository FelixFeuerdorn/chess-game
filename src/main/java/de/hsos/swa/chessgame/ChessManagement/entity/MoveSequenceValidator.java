package de.hsos.swa.chessgame.ChessManagement.entity;

import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MoveSequenceValidator implements ConstraintValidator<ValidMoveSequece, List<ChessMove>> {
    @Inject ChessMoveCheck moveCheck;
    @Inject ChessLogic chessLogic;

    @Override
    public boolean isValid(List<ChessMove> moves, ConstraintValidatorContext context) {
        // Default-Violations sollen nicht erzeugt werden
        context.disableDefaultConstraintViolation();
        boolean result = true;


        //Prüfe, ob die Züge in der gegebenen Reihenfolge ein korrektes Spiel ergeben
        ChessBoard board = new ChessBoard();
        ChessMove lastMove = null;
        int expectedMoveNr = 1;
        for(ChessMove move : moves){
            if(expectedMoveNr != move.getMovenr()){
                context
                    .buildConstraintViolationWithTemplate(
                        "Moves bilden keine durchnummerierte Sequenz.")
                    .addPropertyNode("["+expectedMoveNr+"]")
                    .addConstraintViolation();
                result = false;
                break;
            }
            if(!isConsistentPiece(board, move)){
                context
                    .buildConstraintViolationWithTemplate(
                        "Inconsistent/Incorrect Piece in Move Sequence.")
                    .addPropertyNode("["+expectedMoveNr+"]")
                    .addConstraintViolation();
                result = false;
                break;
            }
            if(!isConsistentColor(board, move)){
                context
                    .buildConstraintViolationWithTemplate(
                        "Inconsistent/Incorrect Coloring of Move Sequence.")
                    .addPropertyNode("["+expectedMoveNr+"]")
                    .addConstraintViolation();
                result = false;
                break;
            }
            if(!moveCheck.check(lastMove, board, move)){
                context
                    .buildConstraintViolationWithTemplate(
                        "Spiel muss aus einer korrekten Reihenfolge " +
                        "von Moves bestehen")
                    .addPropertyNode("["+expectedMoveNr+"]")
                    .addConstraintViolation();
                result = false;
                break;
            }

            chessLogic.makeMove(board, move);
            lastMove = move;
            expectedMoveNr++;
        }

        return result;
    }

    private boolean isConsistentPiece(ChessBoard board, ChessMove move){
        ChessFigure figure = board.getSquare(move.getFrom());
        System.out.println("isConsistentPiece"+figure);
        if(figure == null){ //no such figure...
            return false;
        }

        ChessPiece figurePiece = figure.getChessPiece();
        ChessPiece movePiece = move.getPiece();

        return figurePiece == movePiece;
    }

    private boolean isConsistentColor(ChessBoard board, ChessMove move){
        ChessFigure figure = board.getSquare(move.getFrom());
        if(figure == null){ //no such figure...
            return false;
        }

        ChessColor sequenceColor = move.getMovenr() % 2 == 1 ?
            ChessColor.White : ChessColor.Black;
        ChessColor moveColor = move.getColor();
        ChessColor figureColor = figure.getChessColor();
System.out.println(sequenceColor+" "+moveColor+" "+figureColor);
        return sequenceColor == moveColor && sequenceColor == figureColor;
    }

}
