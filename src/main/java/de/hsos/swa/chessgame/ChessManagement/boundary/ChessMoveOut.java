package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessBoard;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMoveType;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessPiece;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessSquare;
import de.hsos.swa.chessgame.shared.boundary.GameLinkRetriever;
import de.hsos.swa.chessgame.shared.boundary.LinkGenerator;
import de.hsos.swa.chessgame.shared.boundary.UserLinkRetriever;
import de.hsos.swa.chessgame.shared.control.UserAccess;

/**
 * @author Nils Helming
 **/
public class ChessMoveOut {
    public String userName;
    public long gameid;
    public int movenr;
    public ChessSquare from;
    public ChessSquare to;
    public ChessPiece piece;
    public ChessMoveType moveType;
    public ChessPiece promotionPiece;

    public String boardBefore;
    public String boardAfter;

    public LocalDateTime createdAt;

    public Map<String, URI> links;



    @RequestScoped
    public static class Converter{
        @Inject LinkGenerator<ChessMove> moveLinks;
        @Inject UserLinkRetriever userLinks;
        @Inject GameLinkRetriever gameLinks;
        @Inject UserAccess userAccess;

        public ChessMoveOut toDTO(ChessMove move, ChessBoard before, ChessBoard after){
            ChessMoveOut moveOut = new ChessMoveOut();

            moveOut.userName = move.getUser();
            moveOut.gameid = move.getGameid();
            moveOut.movenr = move.getMovenr();
            moveOut.from = move.getFrom();
            moveOut.to = move.getTo();
            moveOut.piece = move.getPiece();
            moveOut.moveType = move.getMoveType();
            moveOut.promotionPiece = move.getPromotionPiece();

            moveOut.boardBefore = BoardStringService.toString(before);
            moveOut.boardAfter = BoardStringService.toString(after);

            moveOut.createdAt = move.getCreatedAt();

            moveOut.links = moveLinks.getEntityLinks(move);
            moveOut.links.put("game",
                gameLinks.getGameLink(move.getGameid()));

            return moveOut;
        }
    }
}
