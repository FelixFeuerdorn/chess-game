package de.hsos.swa.chessgame.ChessManagement.boundary;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessColor;

/**
 * @author Nils Helming
 **/
public class ChessGameIn {
    public String opponent;
    public boolean isPublic;
    public ChessColor hostcolor;
}
