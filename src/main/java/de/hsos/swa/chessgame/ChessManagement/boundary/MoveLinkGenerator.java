package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.UriBuilder;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import de.hsos.swa.chessgame.shared.boundary.LinkGenerator;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class MoveLinkGenerator  implements LinkGenerator<ChessMove>{
    private final long gameid;

    public MoveLinkGenerator(long gameid) {
        this.gameid = gameid;
    }

    public MoveLinkGenerator(){
        this.gameid = -1;
    }

    @Override
    public URI getRequestLink(@Valid @NotNull ChessMove target) {
        return UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "getMove")
                .build(target.getGameid(), target.getMovenr());
    }

    @Override
    public Map<String, URI> getEntityLinks(@Valid @NotNull ChessMove target) {
        Map<String, URI> links = new HashMap<>();

        links.put("self", getRequestLink(target));

        return links;
    }

    @Override
    public URI getPageLink(String pageMethod, int page, int count, Object... buildArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, URI> getCollectionLinks() {
        if(gameid < 1)
            throw new UnsupportedOperationException();

        Map<String, URI> links = new HashMap<>();
        links.put("addZug",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "addMove")
                .build(gameid));

        return links;
    }
}