package de.hsos.swa.chessgame.ChessManagement.entity;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;

/**
 * @author Nils Helming
 **/
public class ChessMove {
    private final String user;
    private final long gameid;
    @Positive private final int movenr;

    @NotNull private final ChessSquare from;
    @NotNull private final ChessSquare to;
    @NotNull @PastOrPresent private final LocalDateTime createdAt;
    @NotNull private final ChessPiece piece;
    @NotNull private final ChessColor color;

    private final ChessMoveType moveType;
    private final ChessPiece promotionPiece;

    public ChessMove(String user, long gameid, int movenr, ChessPiece piece,
            ChessColor color, ChessSquare from, ChessSquare to,
            LocalDateTime createdAt) {
        this.user = user;
        this.gameid = gameid;
        this.movenr = movenr;
        this.from = from;
        this.to = to;
        this.createdAt = createdAt;
        this.piece = piece;
        this.color = color;

        this.moveType = ChessMoveType.NORMAL;
        this.promotionPiece = null;
    }

    public ChessMove(String user, long gameid, int movenr, ChessPiece piece,
            ChessColor color, ChessSquare from, ChessSquare to,
            LocalDateTime createdAt, ChessMoveType moveType,
            ChessPiece promotionPiece){
        this.user = user;
        this.gameid = gameid;
        this.movenr = movenr;
        this.from = from;
        this.to = to;
        this.createdAt = createdAt;
        this.piece = piece;
        this.color = color;

        this.moveType = moveType;
        if(moveType == ChessMoveType.PROMOTION) {
            if(promotionPiece == null)
                throw new IllegalArgumentException("A promotion move must have a promotion piece");
            this.promotionPiece = promotionPiece;
        }else
            this.promotionPiece = null;
    }

    public String getUser() {
        return user;
    }

    public long getGameid() {
        return gameid;
    }

    public int getMovenr(){
        return movenr;
    }

    public ChessSquare getFrom() {
        return from;
    }

    public ChessSquare getTo() {
        return to;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public ChessPiece getPiece() {
        return piece;
    }

    public ChessColor getColor() {
        return color;
    }

    public ChessMoveType getMoveType() {
        return moveType;
    }

    public boolean isCastling() {
        return moveType == ChessMoveType.CASTLING;
    }

    public boolean isEnPassant() {
        return moveType == ChessMoveType.EN_PASSANT;
    }

    public boolean isPromotion() {
        return moveType == ChessMoveType.PROMOTION;
    }

    public ChessPiece getPromotionPiece() {
        return promotionPiece;
    }

    //------------ IMMUTABLE SETTERS ------------

    public ChessMove setUserid(String user) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setGameid(long gameid) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setMovenr(int movenr) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setFrom(ChessSquare from) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setTo(ChessSquare to) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setCreatedAt(LocalDateTime createdAt) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setPiece(ChessPiece piece) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setColor(ChessColor color) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }

    public ChessMove setMoveType(ChessMoveType moveType) {
        return new ChessMove(user, gameid, movenr, piece, color, from, to,
        createdAt, moveType, promotionPiece);
    }
}