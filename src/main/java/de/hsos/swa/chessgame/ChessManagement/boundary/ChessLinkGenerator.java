package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.UriBuilder;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.shared.boundary.GameLinkRetriever;
import de.hsos.swa.chessgame.shared.boundary.LinkGenerator;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class ChessLinkGenerator
        implements LinkGenerator<ChessGame>, GameLinkRetriever{

    @Override
    public URI getGameLink(long gameid) {
        return UriBuilder.fromResource(ChessResource.class)
            .path(ChessResource.class, "getGame")
            .build(gameid);
    }

    @Override
    public URI getRequestLink(@NotNull @Valid ChessGame target) {
        return getGameLink(target.getGameid());
    }

    @Override
    public Map<String, URI> getEntityLinks(@Valid @NotNull ChessGame target) {
        Map<String, URI> links = new HashMap<>();

        links.put("self", getRequestLink(target));
        links.put("getZuege",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "getMoves")
                .build(target.getGameid()));

        links.put("addZug",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "addMove")
                .build(target.getGameid()));

        links.put("update",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "updateGame")
                .build(target.getGameid()));

        links.put("delete",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "deleteGame")
                .build(target.getGameid()));

        return links;
    }

    @Override
    public URI getPageLink(String pageMethod, int page, int count, Object... buildArgs) {
        return UriBuilder.fromResource(ChessResource.class)
            .path(ChessResource.class, pageMethod)
            .queryParam("page", page)
            .queryParam("count", count)
            .build(buildArgs);
    }

    @Override
    public Map<String, URI> getCollectionLinks() {
        Map<String, URI> links = new HashMap<>();

        links.put("create",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "createGame")
                .build());

        links.put("getAllGames",
            UriBuilder.fromResource(ChessResource.class)
                .path(ChessResource.class, "getGamePage")
                .build());

        return links;
    }
}

