package de.hsos.swa.chessgame.ChessManagement.gateway;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMoveType;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessPiece;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessSquare;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

/**
 * @author Nils Helming
 **/
@Entity
public class ChessMoveDB extends PanacheEntity{
    private String userName;
    @ManyToOne
    private ChessGameDB game;
    private int movenr;

    @Enumerated(EnumType.STRING)
    private ChessSquare fromSquare;
    @Enumerated(EnumType.STRING)
    private ChessSquare toSquare;

    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private ChessPiece piece;

    //Special attributes
    @Enumerated(EnumType.STRING)
    private ChessMoveType moveType;
    @Enumerated(EnumType.STRING)
    private ChessPiece promotionPiece = null;

    public ChessMoveDB() {}

    public ChessMoveDB(ChessMove move, ChessGameDB game) {
        this.userName = move.getUser();
        this.game = game;
        this.movenr = move.getMovenr();

        this.fromSquare = move.getFrom();
        this.toSquare = move.getTo();

        this.createdAt = move.getCreatedAt();

        this.piece = move.getPiece();

        this.moveType = move.getMoveType();
        this.promotionPiece = move.getPromotionPiece();
    }

    public String getUser() {
        return userName;
    }

    public void setUser(String user) {
        this.userName = user;
    }

    public ChessGameDB getGame() {
        return game;
    }

    public void setGame(ChessGameDB game) {
        this.game = game;
    }

    public int getMovenr() {
        return movenr;
    }

    public void setMovenr(int movenr) {
        this.movenr = movenr;
    }

    public ChessSquare getFrom() {
        return fromSquare;
    }

    public void setFrom(ChessSquare from) {
        this.fromSquare = from;
    }

    public ChessSquare getTo() {
        return toSquare;
    }

    public void setTo(ChessSquare to) {
        this.toSquare = to;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ChessPiece getPiece() {
        return piece;
    }

    public void setPiece(ChessPiece piece) {
        this.piece = piece;
    }

    public ChessMoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(ChessMoveType moveType) {
        this.moveType = moveType;
    }

    public boolean isCastling() {
        return moveType == ChessMoveType.CASTLING;
    }

    public boolean isEnPassant() {
        return moveType == ChessMoveType.EN_PASSANT;
    }

    public boolean isPromotion() {
        return moveType == ChessMoveType.PROMOTION;
    }

    public ChessPiece getPromotionPiece() {
        return promotionPiece;
    }

    public void setPromotionPiece(ChessPiece promotionPiece) {
        this.promotionPiece = promotionPiece;
    }
}
