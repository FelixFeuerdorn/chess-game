package de.hsos.swa.chessgame.ChessManagement.boundary;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessBoard;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessFigure;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessSquare;

public class BoardStringService {

    public static String toString(ChessFigure figure){
        if(figure == null){
            return " ";
        }
        String character = "";
        switch (figure.getChessPiece()) {
            case Pawn: character = "P"; break;
            case Rook: character = "R"; break;
            case Knight: character = "N"; break;
            case Bishop: character = "B"; break;
            case Queen: character = "Q"; break;
            case King: character = "K"; break;
            default: throw new IllegalArgumentException("Unknown ChessPiece");
        }

        switch (figure.getChessColor()) {
            case White: return character.toUpperCase();
            case Black: return character.toLowerCase();
            default: throw new IllegalArgumentException("Unknown color");
        }
    }

    public static String toString(ChessBoard board){
        StringBuilder sb = new StringBuilder();

        int emptyCount = 0;

        for(int row = ChessSquare.A8.row; row >= ChessSquare.A1.row; row--){
            for(int col = ChessSquare.A1.column; col <= ChessSquare.H8.column; col++){
                ChessSquare square = ChessSquare.giveChessSquare(col, row);
                ChessFigure figure = board.getSquare(square);
                if(figure != null){
                    if(emptyCount > 0){
                        sb.append(emptyCount);
                        emptyCount = 0;
                    }
                    sb.append(toString(figure));
                }else
                    emptyCount++;
            }
            if(emptyCount > 0){
                sb.append(emptyCount);
                emptyCount = 0;
            }
            if(row != ChessSquare.A1.row)
                sb.append("/");
        }
        return sb.toString();
    }
}
