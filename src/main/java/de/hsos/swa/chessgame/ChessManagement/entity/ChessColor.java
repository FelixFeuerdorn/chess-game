package de.hsos.swa.chessgame.ChessManagement.entity;

/**
 * @author Nils Helming
 **/
public enum ChessColor {
    White, Black;

    public static ChessColor getOpposite(ChessColor color) {
        if (color == White) {
            return Black;
        } else {
            return White;
        }
    }
}
