package de.hsos.swa.chessgame.ChessManagement.entity;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;

/**
 * @author Björn Droste
 **/
@RequestScoped
public class ChessMoveCheck {

    private static final int MIN_STEPS = 1;
    private static final int MAX_STEPS = 7;
    private static final int PAWN_STARTSTEPS = 2;

    private ChessBoard chessboard;

    public boolean check(ChessMove lastmove, ChessBoard actualChessboard, ChessMove nextmove) {
        this.chessboard = actualChessboard;

        if (chessboard.getSquare(nextmove.getFrom()) == null) {
            return false;
        }

        // TODO: Methoden checkMate und checkMateFigure implementieren
        List<ChessSquare> mateList = this.checkMate();
        if (!mateList.isEmpty()) {
            boolean matecheck;
            if (chessboard.getSquare(nextmove.getFrom()).getChessPiece() == ChessPiece.King
                    && checkKing(nextmove)) {
                matecheck = checkMateKing(nextmove, mateList);
            } else {
                matecheck = checkMateFigure(nextmove);
            }
            // Kein Schach Konflikt
            if (!matecheck) {
                return false;
            }
        }

        if (nextmove.isEnPassant()) {
            return checkEnPassant(lastmove, nextmove);
        }
        if (nextmove.isPromotion()) {
            return checkPromotion(nextmove);
        }
        if (nextmove.isCastling()) {
            return checkCastling(nextmove);
        }

        switch (nextmove.getPiece()) {
            case Pawn:
                return this.checkPawn(nextmove);
            case Rook:
                return this.checkRook(nextmove);
            case Knight:
                return this.checkKnight(nextmove);
            case Bishop:
                return this.checkBishop(nextmove);
            case Queen:
                return this.checkQueen(nextmove);
            case King:
                return this.checkKing(nextmove);
            default:
                return false;
        }
    }

    public boolean checkEnPassant(ChessMove lastmove, ChessMove nextmove) {
        ChessFigure from = chessboard.getSquare(nextmove.getFrom());
        ChessFigure to = chessboard.getSquare(nextmove.getTo());
        ChessFigure last = chessboard.getSquare(lastmove.getTo());
        if (from == null || to == null || last == null) {
            return false;
        }
        if (from.getChessPiece() != ChessPiece.Pawn
                || to.getChessPiece() != ChessPiece.Pawn
                || last.getChessPiece() != ChessPiece.Pawn
                || !last.getStartState()) {
            return false;
        }
        switch (to.getChessColor()) {
            case White: {
                if (lastmove.getFrom().row - lastmove.getTo().row == PAWN_STARTSTEPS
                        && (nextmove.getFrom().column - 1 == lastmove.getTo().column
                                || lastmove.getFrom().column + 1 == lastmove.getTo().column)) {
                    if (nextmove.getTo().row + 1 == lastmove.getTo().row
                            && nextmove.getTo().column == lastmove.getTo().column) {
                        return true;
                    }
                }
            }
                break;
            case Black: {
                if (lastmove.getTo().row - lastmove.getFrom().row == PAWN_STARTSTEPS
                        && (nextmove.getFrom().column - 1 == lastmove.getTo().column
                                || lastmove.getFrom().column + 1 == lastmove.getTo().column)) {
                    if (nextmove.getTo().row - 1 == lastmove.getTo().row
                            && nextmove.getTo().column == lastmove.getTo().column) {
                        return true;
                    }
                }
            }
                break;
        }
        return false;
    }

    public boolean checkPromotion(ChessMove chessmove) {
        ChessFigure from = chessboard.getSquare(chessmove.getFrom());
        ChessFigure to = chessboard.getSquare(chessmove.getTo());
        if (from == null || to == null) {
            return false;
        }
        if (from.getChessPiece() != ChessPiece.Pawn
                || to.getChessPiece() != ChessPiece.Pawn) {
            return false;
        }
        if (from.getChessColor() != ChessColor.White
                && to.getChessColor() != ChessColor.White
                && chessmove.getFrom().row < 7
                && chessmove.getTo().row < 8) {
            return false;
        } else if (from.getChessColor() != ChessColor.Black
                && to.getChessColor() != ChessColor.Black
                && chessmove.getFrom().row > 2
                && chessmove.getTo().row > 1) {
            return false;
        }
        if (checkSquareBlocked(chessmove)) {
            return false;
        }
        return true;
    }

    public boolean checkCastling(ChessMove chessmove) {
        if (!(chessmove.getFrom() == ChessSquare.E1)
                || !(chessmove.getFrom() == ChessSquare.E8)) {
            return false;
        }
        ChessFigure king = this.chessboard.getSquare(chessmove.getFrom());
        if (king == null) {
            return false;
        } else if (king.getChessPiece() != ChessPiece.King
                || !king.getStartState()) {
            return false;
        }
        king = this.chessboard.getSquare(chessmove.getTo());
        if (king == null) {
            return false;
        } else if (king.getChessPiece() != ChessPiece.King
                || !king.getStartState()) {
            return false;
        }

        switch (chessmove.getTo()) {
            // Weiß Damenseite
            case C1: {
                if (king.getChessColor() == ChessColor.White) {
                    ChessFigure tmpRook = chessboard.getSquare(ChessSquare.A1);
                    if (tmpRook == null) {
                        return false;
                    } else if (tmpRook.getChessPiece() != ChessPiece.Rook
                            || tmpRook.getChessColor() != ChessColor.White
                            || !tmpRook.getStartState()) {
                        return false;
                    }
                }
                if (chessboard.getSquare(ChessSquare.B1) != null
                        && chessboard.getSquare(ChessSquare.C1) != null
                        && chessboard.getSquare(ChessSquare.D1) != null) {
                    return false;
                }
                return true;
            }

            // Weiß Königseite
            case G1: {
                if (king.getChessColor() == ChessColor.White) {
                    ChessFigure tmpRook = chessboard.getSquare(ChessSquare.H1);
                    if (tmpRook == null) {
                        return false;
                    } else if (tmpRook.getChessPiece() != ChessPiece.Rook
                            || tmpRook.getChessColor() != ChessColor.White
                            || !tmpRook.getStartState()) {
                        return false;
                    }
                }
                if (chessboard.getSquare(ChessSquare.F1) != null
                        && chessboard.getSquare(ChessSquare.G1) != null) {
                    return false;
                }
                return true;
            }

            // Schwarz Damenseite
            case C8: {
                if (king.getChessColor() == ChessColor.White) {
                    ChessFigure tmpRook = chessboard.getSquare(ChessSquare.A8);
                    if (tmpRook == null) {
                        return false;
                    } else if (tmpRook.getChessPiece() != ChessPiece.Rook
                            || tmpRook.getChessColor() != ChessColor.Black
                            || !tmpRook.getStartState()) {
                        return false;
                    }
                }
                if (chessboard.getSquare(ChessSquare.B8) != null
                        && chessboard.getSquare(ChessSquare.C8) != null
                        && chessboard.getSquare(ChessSquare.D8) != null) {
                    return false;
                }
                return true;
            }

            // Schwarz Königseite
            case G8: {
                if (king.getChessColor() == ChessColor.White) {
                    ChessFigure tmpRook = chessboard.getSquare(ChessSquare.H8);
                    if (tmpRook == null) {
                        return false;
                    } else if (tmpRook.getChessPiece() != ChessPiece.Rook
                            || tmpRook.getChessColor() != ChessColor.Black
                            || !tmpRook.getStartState()) {
                        return false;
                    }
                }
                if (chessboard.getSquare(ChessSquare.F8) != null
                        && chessboard.getSquare(ChessSquare.G8) != null) {
                    return false;
                }
                return true;
            }
            default:
                return false;
        }
    }

    public boolean checkPawn(ChessMove chessmove) {
        System.out.print("Pawn: ");
        ChessFigure from = chessboard.getSquare(chessmove.getFrom());
        switch (from.getChessColor()) {
            case White: {
                System.out.println("White");
                if (chessmove.getFrom().row < chessmove.getTo().row) {
                    // Zug vom Startpunkt prüfen
                    if (from.getStartState()) {
                        if (chessmove.getFrom().column == chessmove.getTo().column
                                && checkStraightMove(chessmove, PAWN_STARTSTEPS)) {
                            return true;
                        }
                    }

                    // Normaler Zug
                    if (chessmove.getFrom().column == chessmove.getTo().column
                            && checkStraightMove(chessmove, MIN_STEPS)) {
                        return true;
                    }

                    // Gegnerische Figur schlagen
                    if (checkDiagonalMove(chessmove, MIN_STEPS)
                            && this.chessboard.getSquare(chessmove.getTo()) != null) {
                        return true;
                    }
                } else {
                    return false;
                }
            }
                break;

            case Black: {
                if (chessmove.getFrom().row > chessmove.getTo().row) {
                    // Zug vom Startpunkt prüfen
                    if (from.getStartState()) {
                        if (chessmove.getFrom().column == chessmove.getTo().column
                                && checkStraightMove(chessmove, PAWN_STARTSTEPS)) {
                            return true;
                        }
                    }

                    // Normaler Zug
                    if (chessmove.getFrom().column == chessmove.getTo().column
                            && checkStraightMove(chessmove, MIN_STEPS)) {
                        return true;
                    }

                    // Gegnerische Figur schlagen
                    if (checkDiagonalMove(chessmove, MIN_STEPS)
                            && chessmove.getTo() != null) {
                        return true;
                    }
                } else {
                    return false;
                }
            }
                break;
        }
        return false;
    }

    public boolean checkRook(ChessMove chessmove) {
        if (checkStraightMove(chessmove, MAX_STEPS)) {
            return true;
        }
        return false;
    }

    public boolean checkKnight(ChessMove chessmove) {
        ChessFigure knight = chessboard.getSquare(chessmove.getFrom());
        ChessFigure forCheck = chessboard.getSquare(chessmove.getTo());
        if (forCheck == null && knight != null) {
            return true;
        } else if (forCheck != null && knight != null) {
            // Jedes mögliche Feld des Springers prüfen
            if (forCheck.getChessColor() != knight.getChessColor()) {

                if (chessmove.getFrom().column + 2 == chessmove.getTo().column
                        && chessmove.getFrom().row + 1 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column + 2 == chessmove.getTo().column
                        && chessmove.getFrom().row - 1 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column - 2 == chessmove.getTo().column
                        && chessmove.getFrom().row + 1 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column - 2 == chessmove.getTo().column
                        && chessmove.getFrom().row - 1 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column + 1 == chessmove.getTo().column
                        && chessmove.getFrom().row + 2 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column + 1 == chessmove.getTo().column
                        && chessmove.getFrom().row - 2 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column - 1 == chessmove.getTo().column
                        && chessmove.getFrom().row + 2 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }

                if (chessmove.getFrom().column - 1 == chessmove.getTo().column
                        && chessmove.getFrom().row - 2 == chessmove.getTo().row
                        && !checkSquareBlocked(chessmove)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkBishop(ChessMove chessmove) {
        if (checkDiagonalMove(chessmove, MAX_STEPS)) {
            return true;
        }
        return false;
    }

    public boolean checkQueen(ChessMove chessmove) {
        if (checkStraightMove(chessmove, MAX_STEPS)
                || checkDiagonalMove(chessmove, MAX_STEPS)) {
            return true;
        }
        return false;
    }

    public boolean checkKing(ChessMove chessmove) {
        if (checkStraightMove(chessmove, MIN_STEPS)
                || checkDiagonalMove(chessmove, MIN_STEPS)) {
            return true;
        }
        return false;
    }

    private ArrayList<ChessSquare> checkMate() {
        /*
         * TODO: Liste der bedrohten Felder
         * Alle möglichen Felder für den König prüfen und die Liste der Schachfelder zurückgeben
         */
        return new ArrayList<>();
    }

    private boolean checkMateKing(ChessMove chessmove, List<ChessSquare> mateList) {
        for (ChessSquare s : mateList) {
            if (s == chessmove.getTo()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkMateFigure(ChessMove chessmove) {
        /*
         * TODO: Beliebige Figur im Schach
         * Prüfen ob die Figur (Außer König), sich bewegen kann ohne das der König
         * dann im Schach steht.
         */
        return true;
    }

    private boolean checkStraightMove(ChessMove chessmove, int steps) {
        if (chessmove.getFrom().column == chessmove.getTo().column) {
            if (chessmove.getFrom().row - chessmove.getTo().row < 0) {
                // Nach Oben
                for (int i = 1; i <= steps; i++) {
                    ChessMove tmpChessmove = chessmove;
                    if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                            chessmove.getTo().column,
                            chessmove.getFrom().row + i)) == null) {
                        tmpChessmove.setTo(null);
                    } else {
                        tmpChessmove.setTo(ChessSquare.giveChessSquare(
                                chessmove.getTo().column,
                                chessmove.getFrom().row + i));
                    }
                    if (!checkSquareBlocked(tmpChessmove)) {
                        if (chessmove.getTo().row - chessmove.getFrom().row == i) {
                            return true;
                        } else {
                            if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                // Nach Unten
                for (int i = 1; i <= steps; i++) {
                    ChessMove tmpChessmove = chessmove;
                    if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                            chessmove.getTo().column,
                            chessmove.getFrom().row - i)) == null) {
                        tmpChessmove.setTo(null);
                    } else {
                        tmpChessmove.setTo(
                                ChessSquare.giveChessSquare(
                                        chessmove.getTo().column,
                                        chessmove.getFrom().row - i));
                    }
                    if (!checkSquareBlocked(tmpChessmove)) {
                        if (chessmove.getFrom().row - chessmove.getTo().row == i) {
                            return true;
                        } else {
                            if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }
        }
        if (chessmove.getFrom().row == chessmove.getTo().row) {
            if (chessmove.getFrom().column - chessmove.getTo().column < 0) {
                // Nach Rechts
                for (int i = 1; i <= steps; i++) {
                    ChessMove tmpChessmove = chessmove;
                    if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                            chessmove.getFrom().column + i,
                            chessmove.getFrom().row)) == null) {
                        tmpChessmove.setTo(null);
                    } else {
                        tmpChessmove.setTo(ChessSquare.giveChessSquare(
                                chessmove.getFrom().column + i,
                                chessmove.getFrom().row));
                    }
                    if (!checkSquareBlocked(tmpChessmove)) {
                        if (chessmove.getTo().column - chessmove.getFrom().column == i) {
                            return true;
                        } else {
                            if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                // Nach Links
                for (int i = 1; i <= steps; i++) {
                    ChessMove tmpChessmove = chessmove;
                    if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                            chessmove.getFrom().column - i,
                            chessmove.getTo().row)) == null) {
                        tmpChessmove.setTo(null);
                    } else {
                        tmpChessmove.setTo(
                                ChessSquare.giveChessSquare(
                                        chessmove.getFrom().column - i,
                                        chessmove.getTo().row));
                    }
                    if (!checkSquareBlocked(tmpChessmove)) {
                        if (chessmove.getFrom().column - chessmove.getTo().column == i) {
                            return true;
                        } else {
                            if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkDiagonalMove(ChessMove chessmove, int steps) {
        // Nach Rechts-Oben
        if (chessmove.getFrom().row < chessmove.getTo().row
                && chessmove.getFrom().column < chessmove.getTo().column) {

            for (int i = 1; i <= steps; i++) {
                System.out.println(
                        ChessSquare.giveChessSquare(chessmove.getFrom().column + i, chessmove.getFrom().row + i));
                ChessMove tmpChessmove = chessmove;
                if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                        chessmove.getFrom().column + i,
                        chessmove.getFrom().row + i)) == null) {
                    tmpChessmove.setTo(null);
                } else {
                    tmpChessmove.setTo(
                            ChessSquare.giveChessSquare(
                                    chessmove.getFrom().column + i,
                                    chessmove.getFrom().row + i));
                }
                if (!checkSquareBlocked(tmpChessmove)) {
                    if (chessmove.getTo().row - chessmove.getFrom().row == i
                            && chessmove.getTo().column - chessmove.getFrom().column == i) {
                        return true;
                    } else {
                        if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
            // Nach Rechts-Unten
        } else if (chessmove.getFrom().row < chessmove.getTo().row
                && chessmove.getFrom().column > chessmove.getTo().column) {
            for (int i = 1; i <= steps; i++) {
                System.out.println(
                        ChessSquare.giveChessSquare(chessmove.getFrom().column - i, chessmove.getFrom().row + i));
                ChessMove tmpChessmove = chessmove;
                if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                        chessmove.getFrom().column - i,
                        chessmove.getFrom().row + i)) == null) {
                    tmpChessmove.setTo(null);
                } else {
                    tmpChessmove.setTo(
                            ChessSquare.giveChessSquare(
                                    chessmove.getFrom().column - i,
                                    chessmove.getFrom().row + i));
                }
                if (!checkSquareBlocked(tmpChessmove)) {
                    if (chessmove.getTo().row - chessmove.getFrom().row == i
                            && chessmove.getFrom().column - chessmove.getTo().column == i) {
                        return true;
                    } else {
                        if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
            // Nach Links-Oben
        } else if (chessmove.getFrom().row > chessmove.getTo().row
                && chessmove.getFrom().column < chessmove.getTo().column) {
            for (int i = 1; i <= steps; i++) {
                System.out.println(
                        ChessSquare.giveChessSquare(chessmove.getFrom().column + i, chessmove.getFrom().row - i));
                ChessMove tmpChessmove = chessmove;
                if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                        chessmove.getFrom().column + i,
                        chessmove.getFrom().row - i)) == null) {
                    tmpChessmove.setTo(null);
                } else {
                    tmpChessmove.setTo(
                            ChessSquare.giveChessSquare(
                                    chessmove.getFrom().column + i,
                                    chessmove.getFrom().row - i));
                }
                if (!checkSquareBlocked(tmpChessmove)) {
                    if (chessmove.getFrom().row - chessmove.getTo().row == i
                            && chessmove.getTo().column - chessmove.getFrom().column == i) {
                        return true;
                    } else {
                        if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }

            // Nach Links-Unten
        } else if (chessmove.getFrom().row > chessmove.getTo().row
                && chessmove.getFrom().column > chessmove.getTo().column) {
            for (int i = 1; i <= steps; i++) {
                System.out.println(
                        ChessSquare.giveChessSquare(chessmove.getFrom().column - i, chessmove.getFrom().row - i));
                ChessMove tmpChessmove = chessmove;
                if (this.chessboard.getSquare(ChessSquare.giveChessSquare(
                        chessmove.getFrom().column - i,
                        chessmove.getFrom().row - i)) == null) {
                    tmpChessmove.setTo(null);
                } else {
                    tmpChessmove.setTo(
                            ChessSquare.giveChessSquare(
                                    chessmove.getFrom().column - i,
                                    chessmove.getFrom().row - i));
                }
                if (!checkSquareBlocked(tmpChessmove)) {

                    if (chessmove.getFrom().row - chessmove.getTo().row == i
                            && chessmove.getFrom().column - chessmove.getTo().column == i) {
                        return true;
                    } else {
                        if (this.chessboard.getSquare(tmpChessmove.getTo()) != null) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private boolean checkSquareBlocked(ChessMove chessmove) {
        ChessFigure from = chessboard.getSquare(chessmove.getFrom());
        ChessFigure to = chessboard.getSquare(chessmove.getTo());
        if (from != null && to == null) {
            return false;
        } else if (from != null && to != null) {
            // Bauern Speziell prüfen, wegen Gegner-Blockade
            if (from.getChessPiece() == ChessPiece.Pawn
                    && from.getChessColor() == ChessColor.White
                    && chessmove.getFrom().row + 1 == chessmove.getTo().row
                    && chessmove.getFrom().column == chessmove.getTo().column) {
                return true;

            } else if (from.getChessPiece() == ChessPiece.Pawn
                    && from.getChessColor() == ChessColor.Black
                    && chessmove.getFrom().row - 1 == chessmove.getTo().row
                    && chessmove.getFrom().column == chessmove.getTo().column) {
                return true;
            }
            // Typische Prüfung auf Blockade
            if (from.getChessPiece() != to.getChessPiece()
                    && to.getChessPiece() == ChessPiece.King) {
                return false;
            } else if (from.getChessPiece() == to.getChessPiece()
                    && from.getChessColor() != to.getChessColor()
                    && to.getChessPiece() == ChessPiece.King) {
                return false;
            }
            if (from.getChessColor() == to.getChessColor()) {
                return true;
            }
        }
        return false;
    }
}