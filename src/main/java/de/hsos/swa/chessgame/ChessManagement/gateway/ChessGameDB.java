package de.hsos.swa.chessgame.ChessManagement.gateway;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessColor;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

/**
 * @author Nils Helming
 **/
@Entity
public class ChessGameDB extends PanacheEntity {
    //private Long id; //PanacheEntity
    private String hostName;
    private String opponentName;

    @Enumerated(EnumType.STRING)
    private ChessColor hostcolor;
    private boolean isPublic;

    private LocalDateTime createdAt;

    @OneToMany
    private List<ChessMoveDB> moves;

    public ChessGameDB() {} //Panache requires this.

    public ChessGameDB(ChessGame game){
        this.hostName = game.getHost();
        this.opponentName = game.getOpponent();
        this.hostcolor = game.getHostcolor();
        this.isPublic = game.isPublic();

        this.createdAt = game.getCreatedAt();

        this.moves = game.getMoves().stream()
            .map(move -> new ChessMoveDB(move, this))
            .collect(Collectors.toList());
    }

    public long getGameid() {
        return id;
    }

    public String getHost() {
        return hostName;
    }

    public void setHost(String host) {
        this.hostName = host;
    }

    public String getOpponent() {
        return opponentName;
    }

    public void setOpponent(String opponent) {
        this.opponentName = opponent;
    }

    public ChessColor getHostcolor() {
        return hostcolor;
    }

    public void setHostcolor(ChessColor hostcolor) {
        this.hostcolor = hostcolor;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<ChessMoveDB> getMoves() {
        return new ArrayList<>(moves);
    }

    public void addMove(ChessMoveDB move) {
        moves.add(move);
    }
}
