package de.hsos.swa.chessgame.ChessManagement.boundary;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.shared.control.UserAccess;
import io.quarkus.security.identity.SecurityIdentity;

/**
 * @author Nils Helming
 **/
@RequestScoped
public class ChessGameAccess {
    @Inject UserAccess userAccess;

    public boolean userHasId(SecurityIdentity securityIdentity, long userid) {
        if(securityIdentity.isAnonymous()) {
            return false;
        }

        String name = securityIdentity.getPrincipal().getName();
        return userAccess.getUserId(name) == userid;
    }

    public boolean userCanSee(SecurityIdentity securityIdentity, ChessGame chessGame){
        if(securityIdentity.isAnonymous())
            return false;
        else if(securityIdentity.hasRole("ADMIN"))
            return true;
        else if(chessGame.isPublic())
            return true;
        else{
            String name = securityIdentity.getPrincipal().getName();
            long userId = userAccess.getUserId(name);
            if(userId < 0)
                throw new IllegalArgumentException("User not found");
            else if(userAccess.isBanned(userId))
                return false;
            else if(chessGame.getHost().equals(name))
                return true;
            else if(chessGame.getOpponent().equals(name))
                return true;
            return false;
        }
    }

    public boolean userCanModify(SecurityIdentity securityIdentity, ChessGame chessGame){
        if(securityIdentity.isAnonymous())
            return false;
        else if(securityIdentity.hasRole("ADMIN"))
            return true;
        else{
            String name = securityIdentity.getPrincipal().getName();
            long userId = userAccess.getUserId(name);
            if(userId < 0)
                throw new IllegalArgumentException("User not found");
            else if(userAccess.isRestricted(userId))
                return false;
            else if(userAccess.isBanned(userId))
                return false;
            else if(chessGame.getHost().equals(name))
                return true;
            return false;
        }
    }

    public boolean userCanMove(SecurityIdentity securityIdentity, ChessGame chessGame){
        if(securityIdentity.isAnonymous())
            return false;
        else{
            String name = securityIdentity.getPrincipal().getName();
            long userId = userAccess.getUserId(name);

            if(userId < 0)
                throw new IllegalArgumentException("User not found");
            else if(userAccess.isRestricted(userId))
                return false;
            else if(userAccess.isBanned(userId))
                return false;
            else if(chessGame.getHost().equals(name))
                return true;
            else if(chessGame.getOpponent().equals(name))
                return true;
            return false;
        }
    }

    public boolean userCanCreate(SecurityIdentity securityIdentity){
        if(securityIdentity.isAnonymous())
            return false;
        else if(securityIdentity.hasRole("ADMIN"))
            return true;
        else{
            String name = securityIdentity.getPrincipal().getName();
            long userId = userAccess.getUserId(name);
            if(userId < 0)
                throw new IllegalArgumentException("User not found");
            else if(userAccess.isRestricted(userId))
                return false;
            else if(userAccess.isBanned(userId))
                return false;
            else
                return true;
        }
    }
}
