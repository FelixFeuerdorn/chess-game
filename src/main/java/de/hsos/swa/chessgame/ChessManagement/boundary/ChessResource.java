package de.hsos.swa.chessgame.ChessManagement.boundary;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.Transactional;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import de.hsos.swa.chessgame.ChessManagement.control.ChessService;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessBoard;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessLogic;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMoveCheck;
import de.hsos.swa.chessgame.shared.boundary.PageDTO;
import de.hsos.swa.chessgame.shared.boundary.Paginator;
import de.hsos.swa.chessgame.shared.boundary.SelectionDTO;
import de.hsos.swa.chessgame.shared.boundary.Selector;
import de.hsos.swa.chessgame.shared.control.UserAccess;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;

/**
 * @author Nils Helming
 **/
@RequestScoped
@Path("/spiele")
@Transactional(Transactional.TxType.REQUIRES_NEW)
@Produces("application/json")
@Consumes("application/json")
public class ChessResource {

    @Inject ChessService chessService;
    @Inject SecurityIdentity securityIdentity;
    @Inject TransactionManager txManager;
    @Inject UserAccess userAccess;
    @Inject ChessGameAccess gameAccess;

    @Inject ChessGameOut.Converter chessGameOutConverter;
    @Inject ChessMoveOut.Converter chessMoveOutConverter;

    @Inject ChessMoveCheck chessmovecheck;

    @Inject ChessLogic chessLogic;

    @Inject Validator validator;
    private boolean isValidGame(ChessGame game){
        return validator.validate(game).size() == 0;
    }

    @GET
    @Path("")
    @Authenticated
    public Response getGamePage(
            @DefaultValue("1")  @QueryParam("page")  int page,
            @DefaultValue("10") @QueryParam("count") int count) {

        List<ChessGame> games = chessService.getGames().stream()
            .filter(game -> gameAccess.userCanSee(securityIdentity, game))
            .filter(this::isValidGame)
            .collect(Collectors.toList());

        PageDTO<ChessGameOut> pageDTO = Paginator.get(
            games, page, count, new ChessLinkGenerator(), "getGamePage")
            .map(chessGameOutConverter::toDTO);

        return Response.ok(pageDTO).build();
    }

    @GET
    @Path("user/{name}")
    @Authenticated
    public Response getParticipantGames(
            @DefaultValue("1")  @QueryParam("page")  int page,
            @DefaultValue("10") @QueryParam("count") int count,
            @PathParam("name") String opponent) {

        List<ChessGame> games = chessService.getParticipantGames(opponent).stream()
            .filter(game -> gameAccess.userCanSee(securityIdentity, game))
            .filter(this::isValidGame)
            .collect(Collectors.toList());

        PageDTO<ChessGameOut> pageDTO = Paginator.get(games, page, count,
                new ChessLinkGenerator(), "getParticipantGames", opponent)
            .map(chessGameOutConverter::toDTO);

        return Response.ok(pageDTO).build();
    }

    @GET
    @Path("{gameid}")
    @Authenticated
    public Response getGame(@PathParam("gameid") long gameid) {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!isValidGame(game.get())){
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanSee(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        return Response.ok(chessGameOutConverter.toDTO(game.get())).build();
    }

    @GET
    @Path("{gameid}/zuege")
    @Authenticated
    public Response getMoves(
            @DefaultValue("-5") @QueryParam("first") int first,
            @DefaultValue("-1") @QueryParam("last")  int last,
            @PathParam("gameid") long gameid) {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!isValidGame(game.get())){
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanSee(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        SelectionDTO<ChessMoveOut> selectionDTO = Selector.select(
            game.get().getMoves(), first, last, new MoveLinkGenerator(gameid))
            .map(move -> {
                ChessBoard before = chessLogic.generateChessBoard(
                    game.get(), move.getMovenr() - 1);
                ChessBoard after = chessLogic.generateChessBoard(
                    game.get(), move.getMovenr());
                return chessMoveOutConverter.toDTO(move, before, after);
            });

        return Response.ok(selectionDTO).build();
    }

    @GET
    @Path("{gameid}/zuege/{zugnr}")
    @Authenticated
    public Response getMove(
            @PathParam("gameid") long gameid,
            @PathParam("zugnr")  int zugnr) {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!isValidGame(game.get())){
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanSee(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        Optional<ChessMove> move = game.get().getMoves().stream()
            .filter(m -> m.getMovenr() == zugnr)
            .findFirst();

        if(!move.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        ChessBoard before = chessLogic.generateChessBoard(game.get(), zugnr - 1);
        ChessBoard after = chessLogic.generateChessBoard(game.get(), zugnr);

        return Response.ok(
            chessMoveOutConverter.toDTO(move.get(), before, after)).build();
    }

    @POST
    @Path("")
    @Authenticated
    public Response createGame(ChessGameIn chessGame) throws SystemException {

        if(!gameAccess.userCanCreate(securityIdentity)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        long opponentid = userAccess.getUserId(chessGame.opponent);
        if(opponentid < 1) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if(userAccess.isBanned(opponentid)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if(chessGame.hostcolor == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        String username = securityIdentity.getPrincipal().getName();

        Optional<ChessGame> game = chessService.createGame(
            username, chessGame.opponent, chessGame.hostcolor, chessGame.isPublic);

        if(!game.isPresent() || !isValidGame(game.get())) {
            txManager.setRollbackOnly();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok(chessGameOutConverter.toDTO(game.get())).build();
    }

    @POST
    @Path("{gameid}/zuege")
    @Authenticated
    public Response addMove(
            @PathParam("gameid") long gameid,
            ChessMoveIn chessMove) throws SystemException {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!isValidGame(game.get())){
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanMove(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        String nextUser = chessService.getNextMoveUser(gameid);
        long nextUserId = userAccess.getUserId(nextUser);
        if(!gameAccess.userHasId(securityIdentity, nextUserId)) { //it's users turn...
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        game = chessService.addMove(nextUser, gameid, chessMove.from,
            chessMove.to, chessMove.moveType, chessMove.promotionPiece);

        if(!game.isPresent() || !isValidGame(game.get())) {
            txManager.setRollbackOnly();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok(chessGameOutConverter.toDTO(game.get())).build();
    }

    @PUT
    @Path("{gameid}")
    @Authenticated
    public Response updateGame(
            @PathParam("gameid") long gameid,
            ChessGameIn chessGame) throws SystemException {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!isValidGame(game.get())){
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanModify(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if(chessGame.opponent != null) {
            long opponentid = userAccess.getUserId(chessGame.opponent);
            if(opponentid < 0) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            if(userAccess.isBanned(opponentid)) {
                return Response.status(Response.Status.FORBIDDEN).build();
            }

            game = chessService.updateOpponent(gameid, chessGame.opponent);
            if(!game.isPresent() || !isValidGame(game.get())) {
                txManager.setRollbackOnly();
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }

        if(chessGame.hostcolor != null) {
            game = chessService.updateHostColor(gameid, chessGame.hostcolor);
            if(!game.isPresent() || !isValidGame(game.get())) {
                txManager.setRollbackOnly();
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }

        if(chessGame.isPublic != game.get().isPublic()) {
            game = chessService.updatePublic(gameid, chessGame.isPublic);
            if(!game.isPresent() || !isValidGame(game.get())) {
                txManager.setRollbackOnly();
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }

        return Response.ok(chessGameOutConverter.toDTO(game.get())).build();
    }

    @DELETE
    @Path("{gameid}")
    @Authenticated
    public Response deleteGame(@PathParam("gameid") long gameid)
            throws SystemException {

        Optional<ChessGame> game = chessService.getGame(gameid);
        if(!game.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }else if(!gameAccess.userCanModify(securityIdentity, game.get())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        boolean success = chessService.deleteGame(gameid);

        if(!success) {
            txManager.setRollbackOnly();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok().build();
    }
}