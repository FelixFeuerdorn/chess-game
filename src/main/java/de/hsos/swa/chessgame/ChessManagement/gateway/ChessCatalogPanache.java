package de.hsos.swa.chessgame.ChessManagement.gateway;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import de.hsos.swa.chessgame.ChessManagement.entity.ChessCatalog;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessColor;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessGame;
import de.hsos.swa.chessgame.ChessManagement.entity.ChessMove;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

/**
 * @author Nils Helming
 **/
@ApplicationScoped
@Transactional
public class ChessCatalogPanache
        implements ChessCatalog, PanacheRepository<ChessGameDB> {

    private ChessGame toEntity(ChessGameDB gameDB){
        if(gameDB == null)
            return null;

        List<ChessMove> moves = gameDB.getMoves().stream()
            .map(this::toEntity)
            .collect(Collectors.toList());

        return new ChessGame(gameDB.getGameid(), gameDB.getHost(),
            gameDB.getOpponent(), gameDB.getHostcolor(), gameDB.isPublic(),
            gameDB.getCreatedAt(), moves);
    }

    private ChessMove toEntity(ChessMoveDB moveDB){
        if(moveDB == null)
            return null;

        ChessColor color = moveDB.getMovenr() % 2 == 1 ? ChessColor.White : ChessColor.Black;

        return new ChessMove(moveDB.getUser(), moveDB.getGame().getGameid(),
            moveDB.getMovenr(), moveDB.getPiece(), color, moveDB.getFrom(),
            moveDB.getTo(), moveDB.getCreatedAt(), moveDB.getMoveType(),
            moveDB.getPromotionPiece());
    }

    @Override
    public ChessGame createGame(ChessGame game) {
        ChessGameDB gameDB = new ChessGameDB(game);
        gameDB.persist();
        return toEntity(gameDB);
    }

    @Override
    public List<ChessGame> getGames() {
        return this.listAll().stream()
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public ChessGame getGame(long gameid) {
        return toEntity(this.findById(gameid));
    }

    @Override
    public ChessGame updateGameOptions(long gameid, ChessGame update) {
        ChessGameDB gameDB = this.findById(gameid);
        if(gameDB == null)
            return null;

        gameDB.setPublic(update.isPublic());
        gameDB.setOpponent(update.getOpponent());
        gameDB.setHostcolor(update.getHostcolor());

        gameDB.persist();
        return toEntity(gameDB);
    }

    @Override
    public ChessGame addMove(long gameid, ChessMove move) {
        ChessGameDB gameDB = this.findById(gameid);
        if(gameDB == null)
            return null;

        ChessMoveDB moveDB = new ChessMoveDB(move, gameDB);
        gameDB.addMove(moveDB);

        gameDB.persist();
        moveDB.persist();

        return toEntity(gameDB);
    }

    @Override
    public boolean deleteGame(long gameid) {
        return this.deleteById(gameid);
    }
}