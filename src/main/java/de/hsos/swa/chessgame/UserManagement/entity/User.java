package de.hsos.swa.chessgame.UserManagement.entity;

/**
 * @author Björn Droste
 **/
public class User {

    private final long userid;

    private final String username;

    private final String hashedPassword;

    private final GameData gameData;

    private final String role;

    public User(long userid, String username, String hashedPassword, GameData gameData, String role) {
        this.userid = userid;
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.gameData = gameData;
        this.role = role;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gameData == null) ? 0 : gameData.hashCode());
        result = prime * result + ((hashedPassword == null) ? 0 : hashedPassword.hashCode());
        result = prime * result + (int) (userid ^ (userid >>> 32));
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (gameData == null) {
            if (other.gameData != null)
                return false;
        } else if (!gameData.equals(other.gameData))
            return false;
        if (hashedPassword == null) {
            if (other.hashedPassword != null)
                return false;
        } else if (!hashedPassword.equals(other.hashedPassword))
            return false;
        if (userid != other.userid)
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "User [gameData=" + gameData + ", hashedPassword=" + hashedPassword + ", userid=" + userid
                + ", username=" + username + "]";
    }

    public long getUserid() {
        return userid;
    }

    public String getUsername() {
        return username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public GameData getGameData() {
        return gameData;
    }

    public String getRole() {
        return role;
    }
}
