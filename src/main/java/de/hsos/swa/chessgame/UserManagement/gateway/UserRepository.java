package de.hsos.swa.chessgame.UserManagement.gateway;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;

import de.hsos.swa.chessgame.UserManagement.entity.GameData;
import de.hsos.swa.chessgame.UserManagement.entity.Role;
import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.UserManagement.entity.UserCatalog;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.runtime.StartupEvent;

/**
 * @author Björn Droste
 **/
@ApplicationScoped
@Transactional
public class UserRepository implements UserCatalog, PanacheRepository<UserDB> {

    public void initUsers(@Observes StartupEvent e) {
        String password = BcryptUtil.bcryptHash("admin");
        UserDB admin = new UserDB("admin", password, new GameDataDB());
        admin.role = Role.ADMIN.name();
        this.persist(admin);
    }

    @Override
    public User createUser(String username, String password) {
        if (username != null && password != null) {
            UserDB userdb = new UserDB(username, password, new GameDataDB());

            if (userdb != null) {
                this.persist(userdb);
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        new GameData(),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public List<User> readUsers() {
        List<UserDB> userdb = this.listAll();
        return userdb.stream()
                .map(UserDB::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public User readUser(long id) {
        UserDB userdb = this.findById(id);
        if (userdb != null) {
            return new User(userdb.id,
                    userdb.username,
                    userdb.password,
                    GameDataDB.toEntity(userdb.gameDataDB),
                    userdb.role);
        } else {
            return null;
        }
    }

    @Override
    public User readUser(String username) {
        if (username != null) {
            UserDB userdb = this.find("username", username).firstResult();
            if (userdb != null) {
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        GameDataDB.toEntity(userdb.gameDataDB),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public User updateUsername(long id, String username) {
        if (username != null) {
            UserDB userdb = this.findById(id);
            if (userdb != null) {
                userdb.username = username;
                this.persist(userdb);
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        GameDataDB.toEntity(userdb.gameDataDB),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public User updateUserPassword(long id, String password) {
        if (password != null) {
            UserDB userdb = this.findById(id);
            if (userdb != null) {
                userdb.password = password;
                this.persist(userdb);
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        GameDataDB.toEntity(userdb.gameDataDB),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public User updateUserRole(long id, Role role) {
        if (role != null) {
            UserDB userdb = this.findById(id);
            if (userdb != null) {
                userdb.role = role.name();
                this.persist(userdb);
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        GameDataDB.toEntity(userdb.gameDataDB),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public User updateUserGameData(long id, GameData gameData) {
        if (gameData != null) {
            UserDB userdb = this.findById(id);
            if (userdb != null) {
                userdb.gameDataDB = GameDataDB.toDB(gameData);
                this.persist(userdb);
                return new User(userdb.id,
                        userdb.username,
                        userdb.password,
                        GameDataDB.toEntity(userdb.gameDataDB),
                        userdb.role);
            }
        }
        return null;
    }

    @Override
    public boolean deleteUser(long id) {

        UserDB result = this.findById(id);
        if (result != null) {
            boolean check = this.deleteById(id);
            if (check) {
                return true;
            }
        }
        return false;
    }
}
