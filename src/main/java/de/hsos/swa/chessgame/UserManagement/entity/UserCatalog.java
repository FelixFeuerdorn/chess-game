package de.hsos.swa.chessgame.UserManagement.entity;

import java.util.List;

import javax.enterprise.context.RequestScoped;

/**
 * @author Björn Droste
 **/
@RequestScoped
public interface UserCatalog {

    public User createUser(String username, String password);

    public List<User> readUsers();

    public User readUser(long id);

    public User readUser(String username);

    public User updateUsername(long id, String username);

    public User updateUserPassword(long id, String password);

    public User updateUserRole(long id, Role role);

    public User updateUserGameData(long id, GameData gameData);

    public boolean deleteUser(long id);
}
