package de.hsos.swa.chessgame.UserManagement.entity;

/**
 * @author Björn Droste
 **/
public class GameData {

    private final int wins;

    private final int loses;

    private final int draws;

    private final int stalemates;

    private final int castlingKingside;

    private final int castlingQueenside;

    public GameData() {
        this.wins = 0;
        this.loses = 0;
        this.draws = 0;
        this.stalemates = 0;
        this.castlingKingside = 0;
        this.castlingQueenside = 0;
    }

    public GameData(int wins, int loses, int draws, int stalemates, int castlingKingside, int castlingQueenside) {
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
        this.stalemates = stalemates;
        this.castlingKingside = castlingKingside;
        this.castlingQueenside = castlingQueenside;
    }

    public GameData(GameData gamedata){
        this.wins = gamedata.wins;
        this.loses = gamedata.loses;
        this.draws = gamedata.draws;
        this.stalemates = gamedata.stalemates;
        this.castlingKingside = gamedata.castlingKingside;
        this.castlingQueenside = gamedata.castlingQueenside;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + castlingKingside;
        result = prime * result + castlingQueenside;
        result = prime * result + draws;
        result = prime * result + loses;
        result = prime * result + stalemates;
        result = prime * result + wins;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameData other = (GameData) obj;
        if (castlingKingside != other.castlingKingside)
            return false;
        if (castlingQueenside != other.castlingQueenside)
            return false;
        if (draws != other.draws)
            return false;
        if (loses != other.loses)
            return false;
        if (stalemates != other.stalemates)
            return false;
        if (wins != other.wins)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GameData [castlingKingside=" + castlingKingside + ", castlingQueenside=" + castlingQueenside
                + ", draws=" + draws + ", loses=" + loses + ", stalemates=" + stalemates + ", wins=" + wins + "]";
    }

    public int getWins() {
        return wins;
    }

    public int getLoses() {
        return loses;
    }

    public int getDraws() {
        return draws;
    }

    public int getStalemates() {
        return stalemates;
    }

    public int getCastlingKingside() {
        return castlingKingside;
    }

    public int getCastlingQueenside() {
        return castlingQueenside;
    }
}