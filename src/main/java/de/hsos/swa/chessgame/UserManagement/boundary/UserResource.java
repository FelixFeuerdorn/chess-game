package de.hsos.swa.chessgame.UserManagement.boundary;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.hsos.swa.chessgame.UserManagement.control.UserService;
import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.shared.boundary.PageDTO;
import de.hsos.swa.chessgame.shared.boundary.Paginator;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.identity.SecurityIdentity;

/**
 * @author Björn Droste
 **/
@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional(Transactional.TxType.REQUIRES_NEW)
@RequestScoped
public class UserResource {

    @Inject
    UserService userService;

    @Inject
    SecurityIdentity securityIdentity;

    @POST
    @PermitAll
    @Path("")
    public Response createUser(@Valid UserIn userIn) {
        String hashPassword;
        if (userIn.password != null) {
            hashPassword = BcryptUtil.bcryptHash(userIn.password);
        } else {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        Optional<User> user = this.userService.createUser(userIn.username, hashPassword);

        if (!user.isPresent()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok(UserOut.toOut(user.get())).build();
    }

    @GET
    @Path("")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response readUsers(@DefaultValue("1") @QueryParam("page") int page,
            @DefaultValue("20") @QueryParam("count") int count) {

        List<User> users = userService.readUsers().stream()
            .filter(this::hasAccess)
            .collect(Collectors.toList());

        PageDTO<UserOut> pageDTO = Paginator.get(
            users, page, count, new UserLinkGenerator(), "readUsers")
            .map(UserOut::toOut);

        return Response.ok(pageDTO).build();
    }

    @GET
    @Path("/{name}")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response readUserByName(@PathParam("name") String username) {
        Optional<User> user = this.userService.readUser(username);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        UserOut userdto = UserOut.toOut(user.get());
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        return Response.ok(userdto).build();
    }

    @GET
    @Path("/{id}")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response readUserById(@PathParam("id") Long id) {
        Optional<User> user = this.userService.readUser(id);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        return Response.ok(UserOut.toOut(user.get())).build();
    }

    @PUT
    @Path("/{id}/username")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response updateUsername(@PathParam("id") Long id, @Valid UserIn userIn) {
        Optional<User> user = this.userService.readUser(userIn.username);
        if (user.isPresent()) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        user = this.userService.readUser(id);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        user = this.userService.updateUsername(id, userIn.username);

        if (user.isPresent()) {
            return Response.ok(UserOut.toOut(user.get())).build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Path("/{id}/userpassword")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response updateUserPassword(@PathParam("id") Long id, @Valid UserIn userIn) {
        Optional<User> user = this.userService.readUser(id);
        if (user.isPresent()) {
            if (!BcryptUtil.matches(userIn.password, user.get().getHashedPassword())) {
                String hashPassword;
                if (userIn.password != null) {
                    hashPassword = BcryptUtil.bcryptHash(userIn.password);
                    user = this.userService.updateUserPassword(id, hashPassword);
                } else {
                    return Response.status(Response.Status.NOT_ACCEPTABLE).build();
                }
            } else {
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        // später auf userOut umwandeln
        return Response.ok(user).build();
    }

    @PUT
    @Path("/{id}/{userrole}")
    @RolesAllowed({ "ADMIN" })
    public Response updateRole(@PathParam("id") Long id, @PathParam("userrole") String role) {
        System.out.println(role);
        Optional<User> user = this.userService.readUser(id);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        user = this.userService.updateUserRole(id, role);

        if (user.isPresent()) {
            return Response.ok(user.get()/* UserOut.toOut(user.get()) */).build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Path("/{id}/gamedata")
    @RolesAllowed({"ADMIN"})
    public Response updateUserGameData(@PathParam("id") Long id, @Valid GameDataDTO gameDataDTO) {
        Optional<User> user = this.userService.readUser(id);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        user = this.userService.updateUserGameData(id, GameDataDTO.toEntity(gameDataDTO));

        if (user.isPresent()) {
            return Response.ok(UserOut.toOut(user.get())).build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed({ "ADMIN", "USER" })
    public Response deleteUser(@PathParam("id") Long id) {
        Optional<User> user = this.userService.readUser(id);
        if (!user.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        if (!hasAccess(user.get())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        boolean check = false;
        check = this.userService.deleteUser(id);
        if (check) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private boolean hasAccess(User user) {
        if (securityIdentity.isAnonymous()) {
            return false;
        } else if (securityIdentity.hasRole("ADMIN")) {
            return true;
        } else {
            String name = securityIdentity.getPrincipal().getName();
            Optional<User> caller = userService.readUser(name);
            if (caller.isPresent()) {
                return (caller.get().getUserid() == user.getUserid());
            } else {
                return false;
            }
        }
    }
}