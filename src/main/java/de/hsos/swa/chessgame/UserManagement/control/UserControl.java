package de.hsos.swa.chessgame.UserManagement.control;

import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.shared.control.UserAccess;

/**
 * @author Björn Droste
 **/
@RequestScoped
public class UserControl implements UserAccess {

    @Inject
    UserService userService;

    @Override
    public long getUserId(String username) {
        Optional<User> user = userService.readUser(username);
        if (user.isPresent()) {
            return user.get().getUserid();
        } else {
            return -1;
        }
    }

    @Override
    public String getUserName(long userId) {
        Optional<User> user = userService.readUser(userId);
        if (user.isPresent()) {
            return user.get().getUsername();
        } else {
            return null;
        }
    }

    @Override
    public boolean isRestricted(long userId) {
        Optional<User> user = userService.readUser(userId);
        if (user.isPresent()) {
            if(user.get().getRole() == "BOCKED"){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isBanned(long userId) {
        Optional<User> user = userService.readUser(userId);
        if (user.isPresent()) {
            if(user.get().getRole() == "NONE"){
                return true;
            }
        }
        return true;
    }
}