package de.hsos.swa.chessgame.UserManagement.gateway;

import javax.persistence.Embeddable;
import javax.validation.constraints.PositiveOrZero;

import de.hsos.swa.chessgame.UserManagement.entity.GameData;

/**
 * @author Björn Droste
 **/
@Embeddable
public class GameDataDB {

    @PositiveOrZero
    public int wins;

    @PositiveOrZero
    public int loses;

    @PositiveOrZero
    public int draws;

    @PositiveOrZero
    public int stalemates;

    @PositiveOrZero
    public int castlingKingside;

    @PositiveOrZero
    public int castlingQueenside;

    GameDataDB() {
        this.wins = 0;
        this.loses = 0;
        this.draws = 0;
        this.stalemates = 0;
        this.castlingKingside = 0;
        this.castlingQueenside = 0;
    }

    public GameDataDB(int wins, int loses, int draws, int stalemates, int castlingKingside, int castlingQueenside) {
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
        this.stalemates = stalemates;
        this.castlingKingside = castlingKingside;
        this.castlingQueenside = castlingQueenside;
    }

    public static GameDataDB toDB(GameData gameData) {
        return new GameDataDB(gameData.getWins(),
                gameData.getLoses(),
                gameData.getDraws(),
                gameData.getStalemates(),
                gameData.getCastlingKingside(),
                gameData.getCastlingQueenside());
    }

    public static GameData toEntity(GameDataDB gameDataDB) {
        return new GameData(gameDataDB.wins,
                gameDataDB.loses,
                gameDataDB.draws,
                gameDataDB.stalemates,
                gameDataDB.castlingKingside,
                gameDataDB.castlingQueenside);
    }
}