package de.hsos.swa.chessgame.UserManagement.boundary;

import javax.validation.constraints.PositiveOrZero;

import de.hsos.swa.chessgame.UserManagement.entity.GameData;

/**
 * @author Björn Droste
 **/
public class GameDataDTO {

    @PositiveOrZero
    public int wins;

    @PositiveOrZero
    public int loses;

    @PositiveOrZero
    public int draws;

    @PositiveOrZero
    public int stalemates;

    @PositiveOrZero
    public int castlingKingside;

    @PositiveOrZero
    public int castlingQueenside;

    public GameDataDTO() {
    }

    public GameDataDTO(int wins, int loses, int draws, int stalemates, int castlingKingside, int castlingQueenside) {
        this.wins = wins;
        this.loses = loses;
        this.draws = draws;
        this.stalemates = stalemates;
        this.castlingKingside = castlingKingside;
        this.castlingQueenside = castlingQueenside;
    }

    public static GameDataDTO toDTO(GameData gameData) {
        return new GameDataDTO(gameData.getWins(),
                gameData.getLoses(),
                gameData.getDraws(),
                gameData.getStalemates(),
                gameData.getCastlingKingside(),
                gameData.getCastlingQueenside());
    }

    public static GameData toEntity(GameDataDTO gameDataDTO) {
        return new GameData(gameDataDTO.wins,
                gameDataDTO.loses,
                gameDataDTO.draws,
                gameDataDTO.stalemates,
                gameDataDTO.castlingKingside,
                gameDataDTO.castlingQueenside);
    }
}
