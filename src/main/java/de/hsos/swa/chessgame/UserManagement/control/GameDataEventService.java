package de.hsos.swa.chessgame.UserManagement.control;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import de.hsos.swa.chessgame.UserManagement.entity.GameData;
import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.shared.control.GameDataEvent;

/**
 * @author Björn Droste
 **/
@ApplicationScoped
public class GameDataEventService {

    @Inject UserService userService;

    public void updateGameData(@Observes GameDataEvent event){
        String resultname = event.getResultname();
        Optional<User> user = userService.readUser(event.getUserid());
        if(user.isPresent()){
            GameData oldgamedata = user.get().getGameData();
            GameData newgamedata;

            if(oldgamedata != null){
                switch(resultname){
                    case "WIN":{
                        newgamedata = new GameData(
                            oldgamedata.getWins() + event.getValue(),
                            oldgamedata.getLoses(),
                            oldgamedata.getDraws(),
                            oldgamedata.getStalemates(),
                            oldgamedata.getCastlingKingside(),
                            oldgamedata.getCastlingQueenside()
                        );
                    }
                    case "LOSE":{
                        newgamedata = new GameData(
                            oldgamedata.getWins(),
                            oldgamedata.getLoses() + event.getValue(),
                            oldgamedata.getDraws(),
                            oldgamedata.getStalemates(),
                            oldgamedata.getCastlingKingside(),
                            oldgamedata.getCastlingQueenside()
                        );
                    }
                    case "DRAW":{
                        newgamedata = new GameData(
                            oldgamedata.getWins(),
                            oldgamedata.getLoses(),
                            oldgamedata.getDraws() + event.getValue(),
                            oldgamedata.getStalemates(),
                            oldgamedata.getCastlingKingside(),
                            oldgamedata.getCastlingQueenside()
                        );
                    }
                    case "STALEMATES":{
                        newgamedata = new GameData(
                            oldgamedata.getWins(),
                            oldgamedata.getLoses(),
                            oldgamedata.getDraws(),
                            oldgamedata.getStalemates() + event.getValue(),
                            oldgamedata.getCastlingKingside(),
                            oldgamedata.getCastlingQueenside()
                        );
                    }
                    case "KINGSIDECASTLING":{
                        newgamedata = new GameData(
                            oldgamedata.getWins(),
                            oldgamedata.getLoses(),
                            oldgamedata.getDraws(),
                            oldgamedata.getStalemates(),
                            oldgamedata.getCastlingKingside() + event.getValue(),
                            oldgamedata.getCastlingQueenside()
                        );
                    }
                    case "QUEENSIDECASTLING":{
                        newgamedata = new GameData(
                            oldgamedata.getWins(),
                            oldgamedata.getLoses(),
                            oldgamedata.getDraws(),
                            oldgamedata.getStalemates(),
                            oldgamedata.getCastlingKingside(),
                            oldgamedata.getCastlingQueenside() + event.getValue()
                        );
                    }
                    default: newgamedata = new GameData(oldgamedata);
                }

                if(newgamedata != null){
                    userService.updateUserGameData(user.get().getUserid(), newgamedata);
                }
            }
        }
    }
}