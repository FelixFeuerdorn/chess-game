package de.hsos.swa.chessgame.UserManagement.boundary;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import de.hsos.swa.chessgame.UserManagement.entity.User;

/**
 * @author Björn Droste
 **/
public class UserOut {

    @Positive
    public long userid;

    @NotBlank
    @Size(min = 4, max = 32)
    @Pattern(regexp = "^[A-Za-z]*$", message = "Nur Groß- und Kleinscheibung erlaubt")
    public String username;

    @Valid
    public GameDataDTO gameDataDTO;

    public UserOut() {
    }

    public UserOut(long userid, String username, GameDataDTO gameDataDTO) {
        this.userid = userid;
        this.username = username;
        this.gameDataDTO = gameDataDTO;
    }

    public static UserOut toOut(User user) {
        return new UserOut(user.getUserid(), user.getUsername(), GameDataDTO.toDTO(user.getGameData()));
    }
}
