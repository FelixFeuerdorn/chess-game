package de.hsos.swa.chessgame.UserManagement.boundary;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Björn Droste
 **/
public class UserIn {

    @NotBlank
    @Size(min = 4, max = 32)
    @Pattern(regexp = "^[A-Za-z]*$", message = "Nur Groß- und Kleinscheibung erlaubt")
    public String username;

    @NotBlank
    @Size(min = 7, max = 127)
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "Nur Groß- und Kleinscheibung "
            + "erlaubt und Zahlen (max 127 Zeichen)")
    public String password;

    public UserIn() {
    }

    public UserIn(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
