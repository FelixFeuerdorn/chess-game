package de.hsos.swa.chessgame.UserManagement.boundary;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.UriBuilder;

import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.shared.boundary.LinkGenerator;
import de.hsos.swa.chessgame.shared.boundary.UserLinkRetriever;

@RequestScoped
public class UserLinkGenerator
        implements LinkGenerator<User>, UserLinkRetriever {

    @Override
    public URI getUserLink(long userid) {
        return UriBuilder.fromResource(UserResource.class)
            .path(UserResource.class, "readUserById")
            .build(userid);
    }

    @Override
    public URI getRequestLink(@NotNull @Valid User target) {
        return getUserLink(target.getUserid());
    }

    @Override
    public Map<String, URI> getEntityLinks(@Valid @NotNull User target) {
        Map<String, URI> links = new HashMap<>();

        links.put("self", getRequestLink(target));
        links.put("updateUsername",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "updateUsername")
                .build(target.getUserid()));

        links.put("updateRole",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "updateRole")
                .build(target.getUserid()));

        links.put("updateGameData",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "updateUserGameData")
                .build(target.getUserid()));

        links.put("delete",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "deleteUser")
                .build(target.getUserid()));

        return links;
    }

    @Override
    public URI getPageLink(String pageMethod, int page, int count, Object... buildArgs) {
        return UriBuilder.fromResource(UserResource.class)
            .path(UserResource.class, pageMethod)
            .queryParam("page", page)
            .queryParam("count", count)
            .build(buildArgs);
    }

    @Override
    public Map<String, URI> getCollectionLinks() {
        Map<String, URI> links = new HashMap<>();

        links.put("create",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "createUser")
                .build());

        links.put("getAllUsers",
            UriBuilder.fromResource(UserResource.class)
                .path(UserResource.class, "readUsers")
                .build());

        return links;
    }
}
