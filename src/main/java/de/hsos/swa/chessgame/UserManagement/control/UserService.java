package de.hsos.swa.chessgame.UserManagement.control;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import de.hsos.swa.chessgame.UserManagement.entity.GameData;
import de.hsos.swa.chessgame.UserManagement.entity.Role;
import de.hsos.swa.chessgame.UserManagement.entity.User;
import de.hsos.swa.chessgame.UserManagement.entity.UserCatalog;
import de.hsos.swa.chessgame.shared.control.UserDeletedEvent;
import de.hsos.swa.chessgame.shared.control.UserNameChangedEvent;

/**
 * @author Björn Droste
 **/
@RequestScoped
public class UserService {

    @Inject
    UserCatalog userCatalog;

    @Inject
    Event<UserNameChangedEvent> nameChanged;

    @Inject
    Event<UserDeletedEvent> userDeleted;

    public Optional<User> createUser(String username, String password) {
        Optional<User> checkUser = this.readUser(username);

        if (checkUser.isPresent()) {
            return Optional.empty();
        }
        return Optional.ofNullable(userCatalog.createUser(username, password));
    }

    public List<User> readUsers() {
        return this.userCatalog.readUsers();
    }

    public Optional<User> readUser(long id) {
        return Optional.ofNullable(this.userCatalog.readUser(id));
    }

    public Optional<User> readUser(String username) {
        return Optional.ofNullable(this.userCatalog.readUser(username));
    }

    public Optional<User> updateUsername(long id, String username) {

        String oldname = this.userCatalog.readUser(id).getUsername();
        User user = this.userCatalog.updateUsername(id, username);

        if(user != null){
            nameChanged.fire(new UserNameChangedEvent(user.getUserid(), oldname, username));
        }
        return Optional.ofNullable(user);
    }

    public Optional<User> updateUserPassword(long id, String password) {
        return Optional.ofNullable(this.userCatalog.updateUserPassword(id, password));
    }

    public Optional<User> updateUserRole(long id, String role) {
        Role tmpRole;
        switch (role) {
            case "ADMIN":
                tmpRole = Role.ADMIN;
            case "USER":
                tmpRole = Role.USER;
            case "BLOCKED":
                tmpRole = Role.BLOCKED;
            default:
                tmpRole = Role.NONE;
        }
        return Optional.ofNullable(this.userCatalog.updateUserRole(id, tmpRole));
    }

    public Optional<User> updateUserGameData(long id, GameData gameData) {
        return Optional.ofNullable(this.userCatalog.updateUserGameData(id, gameData));
    }

    public boolean deleteUser(long id) {
        User user = this.userCatalog.readUser(id);

        if (user != null) {
            this.userDeleted.fire(new UserDeletedEvent(user.getUserid(), user.getUsername()));
        }

        return this.userCatalog.deleteUser(id);
    }
}
