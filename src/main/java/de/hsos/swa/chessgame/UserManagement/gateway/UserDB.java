package de.hsos.swa.chessgame.UserManagement.gateway;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import de.hsos.swa.chessgame.UserManagement.entity.Role;
import de.hsos.swa.chessgame.UserManagement.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;

/**
 * @author Björn Droste
 **/
@Entity
@UserDefinition
@Table(name = "USERS")
public class UserDB extends PanacheEntity {

    @Username
    @NotBlank
    @Size(min = 4, max = 32)
    @Pattern(regexp = "^[A-Za-z]*$", message = "Nur Groß- und Kleinscheibung erlaubt")
    public String username;

    @Password
    @NotBlank
    public String password;

    @Roles
    public String role;

    @Embedded
    public GameDataDB gameDataDB;

    public UserDB() {
    }

    public UserDB(String username, String passwordbcrypted, GameDataDB gameDataDB) {
        this.username = username;
        this.password = passwordbcrypted;
        this.gameDataDB = gameDataDB;
        this.role = Role.USER.name();
    }

    public static UserDB toDB(User user) {
        return new UserDB(user.getUsername(),
                user.getHashedPassword(),
                GameDataDB.toDB(user.getGameData()));
    }

    public static User toEntity(UserDB userdb) {
        return new User(userdb.id,
                userdb.username,
                userdb.password,
                GameDataDB.toEntity(userdb.gameDataDB),
                userdb.role);
    }
}