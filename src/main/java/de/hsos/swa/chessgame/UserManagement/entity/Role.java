package de.hsos.swa.chessgame.UserManagement.entity;

/**
 * @author Björn Droste
 **/
public enum Role {
    ADMIN,
    USER,
    BLOCKED,
    NONE,
}