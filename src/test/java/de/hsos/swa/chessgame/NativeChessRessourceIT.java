package de.hsos.swa.chessgame;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeChessRessourceIT extends ChessRessourceTest {

    // Execute the same tests but in native mode.
}